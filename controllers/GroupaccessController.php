<?php

namespace app\controllers;

use Yii;
use app\models\Groupaccess;
use app\models\Group;
use app\models\Disk;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

// use yii\filters\VerbFilter;
/**
 * GroupaccessController implements the CRUD actions for Groupaccess model.
 */
class GroupaccessController extends Controller {
    //    /**
    //     * @inheritdoc
    //     */
    //    public function behaviors()
    //    {
    //        return [
    //            'verbs' => [
    //                'class' => VerbFilter::className(),
    //                'actions' => [
    //                    'delete' => ['POST'],
    //                ],
    //            ],
    //        ];
    //    }

    /**
     * Lists all Groupaccess models.
     * @return mixed
     */
    public function actionIndex($groupId) {
        if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = Group::findOne(['groupId' => $groupId]);

        $query = new \yii\db\Query;
        $query->from(['disk' => '{{%disk}}', 'groupaccess' => '{{%groupaccess}}'])
                ->where('disk.diskId=groupaccess.diskId and groupaccess.groupId=' . ( (int) $model->groupId ))
                ->orderBy('disk.diskTitle');
        $disks = $query->all();
        return json_encode($disks);
    }

    /**
     * Lists all Groupaccess models.
     * @return mixed
     */
    public function actionDiskgroups($diskId) {

        if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = Disk::findOne(['diskId' => $diskId]);

        $conditions = ['and'];
        $conditions[] = 'group.groupId=groupaccess.groupId';
        $conditions[] = ' groupaccess.diskId=' . ( (int) $model->diskId );
        if (\Yii::$app->user->identity->is('teacher')) {
            $conditions[] = 'group.creatorId='.( (int)\Yii::$app->user->identity->userId );
        }
        $query = new \yii\db\Query;
        $query->from(['group' => '{{%group}}', 'groupaccess' => '{{%groupaccess}}'])
                ->where($conditions)
                ->orderBy('group.groupName');
        $disks = $query->all();
        return json_encode($disks);
    }

    //    /**
    //     * Displays a single Groupaccess model.
    //     * @param string $groupId
    //     * @param string $diskId
    //     * @return mixed
    //     */
    //    public function actionView($groupId, $diskId)
    //    {
    //        return $this->render('view', [
    //            'model' => $this->findModel($groupId, $diskId),
    //        ]);
    //    }

    /**
     * Creates a new Groupaccess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $post = Yii::$app->request->post();
        if (($model = Groupaccess::findOne(['groupId' => $post['groupId'], 'diskId' => $post['diskId']])) !== null) {
            $model->delete();
        }


        $model = new Groupaccess();
        if ($model->load($post, '') && $model->save()) {
            return json_encode($model->toArray());
        } else {
            return json_encode(['status' => 'error']);
        }
    }

    //    /**
    //     * Updates an existing Groupaccess model.
    //     * If update is successful, the browser will be redirected to the 'view' page.
    //     * @param string $groupId
    //     * @param string $diskId
    //     * @return mixed
    //     */
    //    public function actionUpdate($groupId, $diskId)
    //    {
    //        $model = $this->findModel($groupId, $diskId);
    //
    //        if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //            return $this->redirect(['view', 'groupId' => $model->groupId, 'diskId' => $model->diskId]);
    //        } else {
    //            return $this->render('update', [
    //                'model' => $model,
    //            ]);
    //        }
    //    }

    /**
     * Deletes an existing Groupaccess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $groupId
     * @param string $diskId
     * @return mixed
     */
    public function actionDelete($groupId, $diskId) {

        if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $this->findModel($groupId, $diskId)->delete();

        return json_encode(['status' => 'success']);
    }

    /**
     * Finds the Groupaccess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $groupId
     * @param string $diskId
     * @return Groupaccess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($groupId, $diskId) {
        if (($model = Groupaccess::findOne(['groupId' => $groupId, 'diskId' => $diskId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
