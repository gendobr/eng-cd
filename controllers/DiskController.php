<?php

namespace app\controllers;

use Yii;
use app\models\Disk;
use app\models\DiskSearch;
use app\models\Group;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * DiskController implements the CRUD actions for Disk model.
 */
class DiskController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Disk models.
     * @return mixed
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new DiskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Disk models.
     * @return mixed
     */
    public function actionTeacherindex()
    {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new DiskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('teacherindex', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Disk models.
     * @return mixed
     */
    public function actionStudentindex()
    {

        if (Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException('Access denied');
        }

        $ids = \Yii::$app->user->identity->disks();
        $ids[] = 0;
        $disks = Disk::findAll($ids);
        return $this->render('studentindex', [
                    'disks' => $disks,
        ]);
    }

    /**
     * Displays a single Disk model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->canRead($id)) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);
        $diskrecords = $model->getDiskrecords()->orderBy('diskrecordOrdering')->asArray()->all();


        //$viewname='view2';
        $viewname = 'view3';
        if (isset($_REQUEST['v3'])) {
            $viewname = 'view3';
        }
        return $this->render($viewname, [
                    'model' => $model,
                    'diskrecords' => $diskrecords
        ]);
    }

    /**
     * Creates a new Disk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new Disk();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload_file = $model->uploadFile();
            if ($upload_file !== false) {
                $model->save();
            }
            return $this->redirect(['update', 'id' => $model->diskId]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Disk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $upload_file = $model->uploadFile();
            if ($upload_file !== false) {
                $model->save();
            }
            return $this->redirect(['update', 'id' => $model->diskId]);
        } else {
            // all the groups
            $groups = Group::find()->orderBy('groupName')->asArray()->all();

            $users = User::find()->orderBy('userLastName')->asArray()->all();
            for ($cnt = count($users), $i = 0; $i < $cnt; $i++) {
                $users[$i]['userFullName'] = " {$users[$i]['userLastName']} {$users[$i]['userFirstName']} {$users[$i]['userLogin']}  ";
                $users[$i]['userPassword'] = '';
            }


            return $this->render('update', [
                        'model' => $model,
                        'groups' => $groups,
                        'users' => $users,
            ]);
        }
    }

    public function actionAccess($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new ForbiddenHttpException('Access denied');
        }
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);

        // all the groups
        $groupsQuery = Group::find()->orderBy('groupName');
        if (\Yii::$app->user->identity->is('teacher')) {
            $groupsQuery->andFilterWhere(['creatorId' => \Yii::$app->user->identity->userId]);
        }
        $groups = $groupsQuery->asArray()->all();

        $usersQuery = User::find()->orderBy('userLastName');
        if (\Yii::$app->user->identity->is('teacher')) {
            $usersQuery->andFilterWhere(['creatorId' => \Yii::$app->user->identity->userId]);
        }
        $users = $usersQuery->asArray()->all();
        for ($cnt = count($users), $i = 0; $i < $cnt; $i++) {
            $users[$i]['userFullName'] = " {$users[$i]['userLastName']} {$users[$i]['userFirstName']} {$users[$i]['userLogin']}  ";
            $users[$i]['userPassword'] = '';
        }


        return $this->render('access', [
                    'model' => $model,
                    'groups' => $groups,
                    'users' => $users,
        ]);
    }

    /**
     * Deletes an existing Disk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        if ($model) {
            $model->deleteFile();
            // delete Diskrecords
            $model->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Disk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Disk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Disk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
