<?php

namespace app\controllers;

use Yii;
use app\models\Usergroup;
use app\models\Group;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * UsergroupController implements the CRUD actions for Usergroup model.
 */
class UsergroupController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usergroup models.
     * @return mixed
     */
    public function actionIndex($groupId) {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        if (($model = Group::findOne(['groupId' => $groupId])) !== null) {
            $usersQuery = $model->getUsers()->orderBy('userLastName');
            if (\Yii::$app->user->identity->is('teacher')) {
                $usersQuery->andFilterWhere([
                    'creatorId' => \Yii::$app->user->identity->userId
                ]);
            }

            $users = $usersQuery->asArray()->all();
            for ($i = 0, $cnt = count($users); $i < $cnt; $i++) {
                $users[$i]['userPassword'] = '';
            }
            return json_encode($users);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //    /**
    //     * Displays a single Usergroup model.
    //     * @param string $userId
    //     * @param string $groupId
    //     * @return mixed
    //     */
    //    public function actionView($userId, $groupId)
    //    {
    //        return $this->render('view', [
    //            'model' => $this->findModel($userId, $groupId),
    //        ]);
    //    }

    /**
     * Creates a new Usergroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $post = Yii::$app->request->post();
        $userId = isset($post['userId']) ? $post['userId'] : 0;
        $groupId = isset($post['groupId']) ? $post['groupId'] : 0;
        if (($model = Usergroup::findOne(['userId' => $userId, 'groupId' => $groupId])) !== null) {
            $model->delete();
        }

        $model = new Usergroup();
        if ($model->load($post, '') && $model->save()) {
            return json_encode(['status' => 'success', 'userId' => $model->userId, 'groupId' => $model->groupId]);
        } else {
            return json_encode(['status' => 'error']);
        }
    }

    //    /**
    //     * Updates an existing Usergroup model.
    //     * If update is successful, the browser will be redirected to the 'view' page.
    //     * @param string $userId
    //     * @param string $groupId
    //     * @return mixed
    //     */
    //    public function actionUpdate($userId, $groupId)
    //    {
    //        $model = $this->findModel($userId, $groupId);
    //
    //        if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //            return $this->redirect(['view', 'userId' => $model->userId, 'groupId' => $model->groupId]);
    //        } else {
    //            return $this->render('update', [
    //                'model' => $model,
    //            ]);
    //        }
    //    }

    /**
     * Deletes an existing Usergroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $userId
     * @param string $groupId
     * @return mixed
     */
    public function actionDelete($userId, $groupId) {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($userId, $groupId);
        $userModel = User::findOne(['userId'=>$userId]);
        if(\Yii::$app->user->identity->is('teacher') && $userModel->creatorId != \Yii::$app->user->identity->userId){
            throw new ForbiddenHttpException('Access denied');
        }
        $model->delete();
        return json_encode(['status' => 'success', 'userId' => $userId, 'groupId' => $groupId]);
    }

    /**
     * Finds the Usergroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $userId
     * @param string $groupId
     * @return Usergroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($userId, $groupId) {
        if (($model = Usergroup::findOne(['userId' => $userId, 'groupId' => $groupId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
