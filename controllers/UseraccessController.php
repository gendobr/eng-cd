<?php

namespace app\controllers;

use Yii;
use app\models\Useraccess;
use app\models\User;
use app\models\Disk;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * UseraccessController implements the CRUD actions for Useraccess model.
 */
class UseraccessController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Useraccess models.
     * @return mixed
     */
    public function actionIndex($userId) {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied'); 
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = User::findOne(['userId' => $userId]);
        $query = new \yii\db\Query;
        $query->from(['disk' => '{{%disk}}', 'useraccess' => '{{%useraccess}}'])
                ->where('disk.diskId=useraccess.diskId and useraccess.userId=' . ( (int) $model->userId ))
                ->orderBy('disk.diskTitle');
        $disks = $query->all();
        return json_encode($disks);
    }

    //    /**
    //     * Displays a single Useraccess model.
    //     * @param string $userId
    //     * @param string $diskId
    //     * @return mixed
    //     */
    //    public function actionView($userId, $diskId)
    //    {
    //        return $this->render('view', [
    //            'model' => $this->findModel($userId, $diskId),
    //        ]);
    //    }

    public function actionDiskusers($diskId) {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied'); 
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = Disk::findOne(['diskId' => $diskId]);
        
        $conditions = ['and'];
        $conditions[] = 'user.userId=useraccess.userId';
        $conditions[] = ' useraccess.diskId=' . ( (int) $model->diskId );
        if (\Yii::$app->user->identity->is('teacher')) {
            $conditions[] = 'user.creatorId='.( (int)\Yii::$app->user->identity->userId );
        }

        
        $query = new \yii\db\Query;
        $query->from(['user' => '{{%user}}', 'useraccess' => '{{%useraccess}}'])
                ->where($conditions)
                ->orderBy('user.userLastName');
        $users = $query->all();
        for ($i = 0, $cnt = count($users); $i < $cnt; $i++) {
            $users[$i]['userPassword'] = '';
            $users[$i]['userTemporaryKey'] = '';
            $users[$i]['userToken'] = '';
        }
        return json_encode($users);
    }

    /**
     * Creates a new Useraccess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied'); 
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $post = Yii::$app->request->post();
        if (($model = Useraccess::findOne(['userId' => $post['userId'], 'diskId' => $post['diskId']])) !== null) {
            $model->delete();
        }


        $model = new Useraccess();
        if ($model->load($post, '') && $model->save()) {
            return json_encode($model->toArray());
        } else {
            return json_encode(['status' => 'error']);
        }
    }

    //    /**
    //     * Updates an existing Useraccess model.
    //     * If update is successful, the browser will be redirected to the 'view' page.
    //     * @param string $userId
    //     * @param string $diskId
    //     * @return mixed
    //     */
    //    public function actionUpdate($userId, $diskId)
    //    {
    //        $model = $this->findModel($userId, $diskId);
    //
    //        if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //            return $this->redirect(['view', 'userId' => $model->userId, 'diskId' => $model->diskId]);
    //        } else {
    //            return $this->render('update', [
    //                'model' => $model,
    //            ]);
    //        }
    //    }

    /**
     * Deletes an existing Useraccess model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $userId
     * @param string $diskId
     * @return mixed
     */
    public function actionDelete($userId, $diskId) {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied'); 
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $this->findModel($userId, $diskId)->delete();

        return json_encode(['status' => 'success']);
    }

    /**
     * Finds the Useraccess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $userId
     * @param string $diskId
     * @return Useraccess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($userId, $diskId) {
        if (($model = Useraccess::findOne(['userId' => $userId, 'diskId' => $diskId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
