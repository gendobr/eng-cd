<?php

namespace app\controllers;

use Yii;
use app\models\Diskrecord;
use app\models\Disk;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

// use yii\filters\VerbFilter;

/**
 * DiskrecordController implements the CRUD actions for Diskrecord model.
 */
class DiskrecordController extends Controller {
    //    /**
    //     * @inheritdoc
    //     */
    //    public function behaviors() {
    //        return [
    //            'verbs' => [
    //                'class' => VerbFilter::className(),
    //                'actions' => [
    //                    'delete' => ['POST'],
    //                ],
    //            ],
    //        ];
    //    }

    /**
     * Lists all Diskrecord models.
     * @return mixed
     */
    public function actionIndex($diskId) {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        if (($disk = Disk::findOne($diskId)) !== null) {
            $records = $disk->getDiskrecords()->orderBy('diskrecordOrdering')->asArray()->all();
            return json_encode($records);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //    /**
    //     * Displays a single Diskrecord model.
    //     * @param string $id
    //     * @return mixed
    //     */
    //    public function actionView($id) {
    //        $model = $this->findModel($id);
    //        return json_encode($model->toArray());
    //    }

    /**
     * Creates a new Diskrecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }


        $post = Yii::$app->request->post();
        //return json_encode($_FILES);
        // print_r($_FILES);

        $model = new Diskrecord();
        if ($model->load($post, '') && $model->save()) {
            $uploadMp3 = $model->uploadMp3();
            $uploadOgg = $model->uploadOgg();
            $uploadWav = $model->uploadWav();
            if ($uploadMp3 !== false || $uploadOgg !== false || $uploadWav !== false) {
                $model->save();
            }
            return json_encode($model->toArray());
        } else {
            return json_encode(['status' => 'error', 'messages' => $model->errors]);
        }
    }

    /**
     * Updates an existing Diskrecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            $uploadMp3 = $model->uploadMp3();
            $uploadOgg = $model->uploadOgg();
            $uploadWav = $model->uploadWav();
            if ($uploadMp3 !== false || $uploadOgg !== false || $uploadWav !== false) {
                $model->save();
            }
            return json_encode($model->toArray());
        } else {
            return json_encode($model->toArray());
        }
    }

    /**
     * Deletes an existing Diskrecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        if ($model) {
            $model->deleteMp3();
            $model->deleteWav();
            $model->deleteOgg();
            $model->delete();
            return json_encode($model->toArray());
        }
        return json_encode(['status' => 'delete error']);
    }

    /**
     * Redirect to simple static file server
     * accepts URL like 
     * /[secure_code]/[file_path]
     * secure_code is md5(salt+file_path+timestamp).timestamp
     * timestamp is link expiration time, number of seconds since 01.01.1970
     * 
     * modified code from
     * http://www.hongkiat.com/blog/node-js-server-side-javascript/
     * using md5 function http://www.myersdaily.org/joseph/javascript/md5.js
     */
    public function actionStream($diskrecordId, $file) {
        $model = $this->findModel($diskrecordId);
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->canRead($model->diskId)) {
            throw new ForbiddenHttpException('Access denied');
        }
        switch (strtolower($file)) {
            case 'mp3':
		header("Content-Type: audio/mpeg");
                $file_path = $model->diskrecordFilePathMp3;
                break;

            case 'm4a':
		header("Content-Type: audio/mp4");
                $file_path = preg_replace("/mp3\$/","m4a",$model->diskrecordFilePathMp3);
                break;

            case 'ogg':
		header("Content-Type: audio/ogg");
                $file_path = $model->diskrecordFilePathOgg;
                break;
            case 'wav':
		header("Content-Type: audio/wav");
                $file_path = $model->diskrecordFilePathWav;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist.');
        }
        $timestamp = time() + \Yii::$app->params['link_expiration_interval'];
        $secure_code = md5(\Yii::$app->params['salt'] . '/'.$file_path . $timestamp);
        $secure_link = \Yii::$app->params['file_server_host'] . "/{$secure_code}.{$timestamp}/{$file_path}";
        header("Location: $secure_link");
        exit();
    }

    /**
     * Finds the Diskrecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Diskrecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Diskrecord::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
