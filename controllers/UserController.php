<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\LoginForm;
use app\models\UserBatchForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionTeacherindex() {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // print_r($dataProvider->query);
        $dataProvider->query->andFilterWhere([
            'userLevel' => User::$userLevelOptionsInverted['Student'],
            'creatorId' => \Yii::$app->user->identity->userId
        ]);

        return $this->render('teacherindex', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->is('teacher') && $model->userLevel != User::$userLevelOptionsInverted['Student']) {
            throw new ForbiddenHttpException('Access denied');
        }
        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }

        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new User();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {

            // teacher can create students
            if (\Yii::$app->user->identity->is('teacher')) {
                $model->userLevel = User::$userLevelOptionsInverted['Student'];
            }

            $model->creatorId = \Yii::$app->user->identity->userId;

            if ($model->save()) {
                // save new password
                if (isset($post['password']) && $post['password'][0] && $post['password'][0] == $post['password'][1]) {
                    $model->setPassword($post['password'][0]);
                    $model->save();
                }

                return $this->redirect(['view', 'id' => $model->userId]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        // teacher can manage only students
        if (\Yii::$app->user->identity->is('teacher') && $model->userLevel != User::$userLevelOptionsInverted['Student']) {
            throw new ForbiddenHttpException('Access denied');
        }
        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }

        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            // save new password
            if (isset($post['password']) && $post['password'][0]) {

                if ($post['password'][0] == $post['password'][1]) {
                    $model->setPassword($post['password'][0]);
                    $model->save();
                } else {
                    $model->addError('userPassword', $error = 'Type identical passwords, please');
                    return $this->render('update', [
                                'model' => $model,
                    ]);
                }
            }
            return $this->redirect(['view', 'id' => $model->userId]);
        } else {

            $query = new \yii\db\Query;
            $query->from(['disk' => '{{%disk}}'])
                    ->orderBy('disk.diskTitle');
            $disks = $query->all();


            return $this->render('update', [
                        'model' => $model,
                        'disks' => $disks
            ]);
        }
    }

    /**
     * batch user processing
     */
    public function actionBatch() {
	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $message = '';
        $post = Yii::$app->request->post();
        if (isset($post['csv']) && $post['csv']) {
            $model = new UserBatchForm();

            $model->delimiter = $post['delimiter'];
            $model->enclosure = $post['enclosure'];
            $model->escape = $post['escape'];

            $model->load_csv($post['csv']);

            return $this->render('batch-02-rows', [
                        'message' => $message,
                        'model' => $model
            ]);
        } elseif (isset($post['rows']) && $post['rows']) {
            $model = new UserBatchForm();
            $model->load_rows($post['fields'], $post['rows']);
            // print_r($model);

            $report = $model->executeBatch();
            // print_r($report);exit();
            return $this->render('batch-03-report', [
                        'message' => $message,
                        'report' => $report,
                        'model' => $model
            ]);
        } else {
            $model = new UserBatchForm();
            return $this->render('batch-01-input', [
                        'message' => $message,
                        'model' => $model
            ]);
        }
    }

    public function actionPasswd($key) {

        $message = '';
        $query = User::find();
        $query->andFilterWhere(['userTemporaryKey' => $key]);
        $userModel = $query->one();
        if ($userModel) {
            $model = $userModel;
            $post = Yii::$app->request->post();
            if (isset($post['password']) && $post['password'][0]) {
                if ($post['password'][0] == $post['password'][1]) {
                    $userModel->setPassword($post['password'][0]);
                    $userModel->userTemporaryKey = '';
                    $userModel->save();
                    $model = new User();
                    $message = 'Password updated';

                    $model = new LoginForm();
                    return $this->render('//site/login', [
                                'message' => $message,
                                'model' => $model,
                    ]);
                } else {
                    $message = 'Type identical passwords, please';
                }
            } else {
                $message = '';
            }
        } else {
            $model = new User();
            $message = 'Invalid secure key';
        }

        return $this->render('passwd', [
                    'message' => $message,
                    'model' => $model,
                    'key' => $key
        ]);
    }

    public function actionRecovery() {
        $message = '';
        $post = Yii::$app->request->post();
        if ($post && isset($post['User']) && isset($post['User']['userEMail'])) {

            $query = User::find();
            $query->andFilterWhere([
                'userEMail' => $post['User']['userEMail'],
            ]);
            $userModel = $query->one();
            if ($userModel) {
                // create password recovery link
                $userModel->userTemporaryKey = base64_encode(md5(time() . rand(0, 100001)));
                $userModel->save();

                //$secureUrl = Yii::$app->homeUrl . '?r=user/passwd&key=' . ($userModel->userTemporaryKey);
                $secureUrl = \yii\helpers\Url::home(true) . '?r=user/passwd&key=' . ($userModel->userTemporaryKey);

                // and mail it to user
                $message = 'Secure link is sent to your email';
                // print_r($message);
                Yii::$app->mailer->compose([
                            'html' => 'user/recoveryEmailHtml',
                            'text' => 'user/recoveryEmailText',
                                ], [
                            'secureUrl' => $secureUrl,
                            'model' => $userModel
                        ])
                        ->setFrom(Yii::$app->params['adminEmail'])
                        ->setTo($userModel->userEMail)
                        ->setSubject('Password Recovery')
                        ->send();
            } else {
                $model = new User();
                $model->userEMail = $post['User']['userEMail'];
                $message = 'Email not found';
                return $this->render('recovery', [
                            'message' => $message,
                            'model' => $model
                ]);
            }
        }
        // draw recovery form

        $model = new User();

        return $this->render('recovery', [
                    'message' => $message,
                    'model' => $model
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {

	if(Yii::$app->user->isGuest){
	    throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->is('teacher') && $model->userLevel != User::$userLevelOptionsInverted['Student']) {
            throw new ForbiddenHttpException('Access denied');
        }
        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model->delete();
        if (\Yii::$app->user->identity->is('admin')) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['teacherindex']);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
