<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\GroupSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('admin')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionTeacherindex() {

        if (Yii::$app->user->isGuest || !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere([
            'creatorId' => \Yii::$app->user->identity->userId
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
	if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }

        $users = $model->getUsers()->orderBy('userLastName')->asArray()->all();

        $query = new \yii\db\Query;
        $query->from(['disk' => '{{%disk}}', 'groupaccess' => '{{%groupaccess}}'])
                ->where('disk.diskId=groupaccess.diskId and groupaccess.groupId=' . ( (int) $model->groupId ))
                ->orderBy('disk.diskTitle');
        $disks = $query->all();

        return $this->render('view', [
                    'model' => $model,
                    'users' => $users,
                    'disks' => $disks,
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

	if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new Group();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->creatorId = \Yii::$app->user->identity->userId;
            $model->save();

            return $this->redirect(['view', 'id' => $model->groupId]);
        } else {
            return $this->render('create', ['model' => $model,]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {

	if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }



        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->groupId]);
        } else {

            $query = new \yii\db\Query;
            $query->from(['disk' => '{{%disk}}'])
                    ->orderBy('disk.diskTitle');
            $disks = $query->all();

            $userQuery = User::find()->orderBy('userLastName');
            if (\Yii::$app->user->identity->is('teacher')) {
                $userQuery->andFilterWhere(['creatorId' => \Yii::$app->user->identity->userId]);
            }
            // echo "****************";
            // print_r($userQuery->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            $users = $userQuery->asArray()->all();
            for ($cnt = count($users), $i = 0; $i < $cnt; $i++) {
                $users[$i]['userFullName'] = " {$users[$i]['userLastName']} {$users[$i]['userFirstName']} {$users[$i]['userLogin']}  ";
                $users[$i]['userPassword'] = '';
            }

            return $this->render('update', [
                        'model' => $model,
                        'disks' => $disks,
                        'students' => $users
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {

	if(Yii::$app->user->isGuest){
            throw new ForbiddenHttpException('Access denied');
	}
        if (!\Yii::$app->user->identity->is('admin') && !\Yii::$app->user->identity->is('teacher')) {
            throw new ForbiddenHttpException('Access denied');
        }
        $model = $this->findModel($id);

        if (\Yii::$app->user->identity->is('teacher') && \Yii::$app->user->identity->userId != $model->creatorId) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
