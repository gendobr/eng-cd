/*
SQLyog Community v12.4.3 (32 bit)
MySQL - 5.7.18-0ubuntu0.16.04.1 : Database - cd
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cd` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `cd`;

/*Table structure for table `cd_disk` */

DROP TABLE IF EXISTS `cd_disk`;

CREATE TABLE `cd_disk` (
  `diskId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `diskTitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `diskIcon` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `diskDescription` text COLLATE utf8_bin,
  PRIMARY KEY (`diskId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_disk` */

insert  into `cd_disk`(`diskId`,`diskTitle`,`diskIcon`,`diskDescription`) values 
(1,'Disk 1','uploads/1/cover.jpg','some disk description'),
(2,'Disk 2','uploads/2/cover.jpg','some disk description'),
(3,'Disk 3','','some disk description'),
(4,'Disk 4','','some disk description'),
(5,'Disk 5','','some disk description'),
(6,'Disk 6','','some disk description'),
(7,'Disk 7','','some disk description'),
(8,'Disk 8','','some disk description'),
(9,'Disk 9','','some disk description'),
(10,'Disk 10',NULL,'some disk description'),
(11,'Disk 11','uploads/11/cover.jpg','some disk description');

/*Table structure for table `cd_diskrecord` */

DROP TABLE IF EXISTS `cd_diskrecord`;

CREATE TABLE `cd_diskrecord` (
  `diskrecordId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `diskrecordFilePathMp3` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `diskrecordFilePathOgg` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `diskrecordFilePathWav` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `diskrecordTitle` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `diskId` bigint(20) unsigned NOT NULL,
  `diskrecordOrdering` int(11) DEFAULT NULL,
  PRIMARY KEY (`diskrecordId`),
  KEY `diskId` (`diskId`),
  CONSTRAINT `cd_diskrecord_ibfk_1` FOREIGN KEY (`diskId`) REFERENCES `cd_disk` (`diskId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_diskrecord` */

insert  into `cd_diskrecord`(`diskrecordId`,`diskrecordFilePathMp3`,`diskrecordFilePathOgg`,`diskrecordFilePathWav`,`diskrecordTitle`,`diskId`,`diskrecordOrdering`) values 
(7,'uploads/1/track.7.Track_1.mp3','uploads/1/track.7.Track_1.ogg','uploads/1/track.7.Track_1.wav','track 01',1,1),
(10,'uploads/1/track.10.Track_2.mp3','uploads/1/track.10.Track_2.ogg','uploads/1/track.10.Track_2.wav','track 02',1,2),
(11,'uploads/2/track.11.Track_1.mp3','uploads/2/track.11.Track_1.ogg','uploads/2/track.11.Track_1.wav','Track 01',2,1),
(12,'uploads/2/track.12.Track_2.mp3','uploads/2/track.12.Track_2.ogg','uploads/2/track.12.Track_2.wav','Track 02',2,2),
(13,'uploads/2/track.13.Track_3.mp3','uploads/2/track.13.Track_3.ogg','uploads/2/track.13.Track_3.wav','Track 03',2,3);

/*Table structure for table `cd_group` */

DROP TABLE IF EXISTS `cd_group`;

CREATE TABLE `cd_group` (
  `groupId` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `groupDescription` text COLLATE utf8_bin,
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_group` */

insert  into `cd_group`(`groupId`,`groupName`,`groupDescription`) values 
(1,'class No1','class No1 description');

/*Table structure for table `cd_groupaccess` */

DROP TABLE IF EXISTS `cd_groupaccess`;

CREATE TABLE `cd_groupaccess` (
  `groupId` bigint(20) unsigned NOT NULL,
  `diskId` bigint(20) unsigned NOT NULL,
  `fromDate` date DEFAULT NULL,
  `toDate` date DEFAULT NULL,
  PRIMARY KEY (`groupId`,`diskId`),
  KEY `diskId` (`diskId`),
  CONSTRAINT `cd_groupaccess_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `cd_group` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cd_groupaccess_ibfk_2` FOREIGN KEY (`diskId`) REFERENCES `cd_disk` (`diskId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_groupaccess` */

insert  into `cd_groupaccess`(`groupId`,`diskId`,`fromDate`,`toDate`) values 
(1,1,'2017-07-28','2017-09-29'),
(1,6,'2017-07-02','2018-04-28'),
(1,7,'2017-07-01','2019-03-08'),
(1,8,'2017-07-02','2018-11-23');

/*Table structure for table `cd_user` */

DROP TABLE IF EXISTS `cd_user`;

CREATE TABLE `cd_user` (
  `userId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userLogin` varchar(50) DEFAULT NULL,
  `userPassword` varchar(50) DEFAULT NULL,
  `userFirstName` varchar(128) DEFAULT NULL,
  `userLastName` varchar(128) DEFAULT NULL,
  `userEMail` varchar(128) DEFAULT NULL,
  `userLastAccess` int(10) DEFAULT '0',
  `userAccessDuration` int(10) DEFAULT '0',
  `userTemporaryKey` varchar(128) DEFAULT NULL,
  `userIsValid` tinyint(1) NOT NULL DEFAULT '0',
  `userIsBanned` tinyint(1) NOT NULL DEFAULT '0',
  `userIsActive` tinyint(1) NOT NULL DEFAULT '0',
  `userTelephone` varchar(128) DEFAULT NULL,
  `userLevel` int(11) NOT NULL DEFAULT '0',
  `userToken` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `UserLogin` (`userLogin`),
  KEY `sysuserId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `cd_user` */

insert  into `cd_user`(`userId`,`userLogin`,`userPassword`,`userFirstName`,`userLastName`,`userEMail`,`userLastAccess`,`userAccessDuration`,`userTemporaryKey`,`userIsValid`,`userIsBanned`,`userIsActive`,`userTelephone`,`userLevel`,`userToken`) values 
(1,'eng-cd','dsKO3qoZ9iTww','admin','admin','test@email.com',1501015953,0,'YTljNWViM2Q2ZTdiZWNmZWMxZjA3ZjQ3MGUyN2EwMzI=',0,0,0,'',4,'1234567890987654321'),
(2,'teacher','ds9EGnjnxLb0w','Teaher','Teacher','teacher@email.com',0,0,NULL,1,0,0,'1234567890',3,NULL),
(3,'student','dsR1qDE1A4nEg','Student','Student','student@email.com',0,0,NULL,1,0,0,'1234567890',2,NULL),
(21,'test1@mail.com','ds6PR8qNuAEzM','Some First Name','Some Last Name','test1@mail.com',0,0,NULL,1,0,1,'+3801234567890',4,NULL);

/*Table structure for table `cd_useraccess` */

DROP TABLE IF EXISTS `cd_useraccess`;

CREATE TABLE `cd_useraccess` (
  `userId` bigint(20) unsigned NOT NULL,
  `diskId` bigint(20) unsigned NOT NULL,
  `fromDate` date DEFAULT NULL,
  `toDate` date DEFAULT NULL,
  PRIMARY KEY (`userId`,`diskId`),
  KEY `diskId` (`diskId`),
  CONSTRAINT `cd_useraccess_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `cd_user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cd_useraccess_ibfk_2` FOREIGN KEY (`diskId`) REFERENCES `cd_disk` (`diskId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_useraccess` */

insert  into `cd_useraccess`(`userId`,`diskId`,`fromDate`,`toDate`) values 
(1,2,'2017-07-01','2017-12-31'),
(3,1,'2017-07-26','2017-07-31'),
(21,1,'2017-06-30','2017-07-31');

/*Table structure for table `cd_usergroup` */

DROP TABLE IF EXISTS `cd_usergroup`;

CREATE TABLE `cd_usergroup` (
  `userId` bigint(20) unsigned NOT NULL,
  `groupId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`userId`,`groupId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `cd_usergroup_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `cd_user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cd_usergroup_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `cd_group` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `cd_usergroup` */

insert  into `cd_usergroup`(`userId`,`groupId`) values 
(3,1),
(21,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
