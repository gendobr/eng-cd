<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * UserBatchForm is the model behind the login form.
 *
 *
 */
class UserBatchForm extends Model {

    public $csv;
    // public $password;
    // public $rememberMe = true;
    public $fields = [];
    public $rows = [];
    public $delimiter = "\\t";
    public $enclosure = '';
    public $escape = "\\";
    private $fieldsInverse;
    private $map=["\\t"=>"\t"];

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            // [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            // ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            // ['password', 'validatePassword'],
            [['csv', 'delimiter', 'enclosure', 'escape'], 'string'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function load_csv($csv) {
        // print_r($this);
        $this->csv = $csv;

        // parse csv into fields and rows
        // the rows
        $tmp = explode("\n", $csv);
        // print_r($tmp);
        
        $this->delimiter = str_replace(array_keys($this->map), array_values($this->map), $this->delimiter);
        $this->enclosure = str_replace(array_keys($this->map), array_values($this->map), $this->enclosure);
        $this->escape    = str_replace(array_keys($this->map), array_values($this->map), $this->escape   );

        $this->fields = str_getcsv($tmp[0], $this->delimiter, $this->enclosure, $this->escape);
        $this->fieldsInverse = array_flip($this->fields);

        $this->rows = [];
        for ($cnt = count($tmp), $i = 1; $i < $cnt; $i++) {
            $tmp[$i]=trim($tmp[$i]);
            if($tmp[$i]){
                $this->rows[] = str_getcsv($tmp[$i], $this->delimiter, $this->enclosure, $this->escape);
            }
        }
    }

    public function load_rows($fieldsJson, $rowsJson) {
        $this->fields = json_decode($fieldsJson);
        $this->fieldsInverse = array_flip($this->fields);
        $this->rows = json_decode($rowsJson);
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }

    public function executeBatch() {
        $report = [];
        foreach ($this->rows as $rowId => $row) {
            $report[$rowId] = $this->executeBatchRow($row);
        }
        return $report;
    }

    public function executeBatchRow($row) {
        $action = strtolower($this->get('action', $row));
        switch ($action) {
            case 'add':
            case 'new':
            case 'insert':
            case 'create':
                return $this->addRow($row);

            case 'change':
            case 'update':
            case 'set':
            case 'replace':
                return $this->updateRow($row);
                // return 'update - OK';
                
            case 'del':
            case 'rm':
            case 'delete':
            case 'remove':
            case 'trash':
                return $this->deleteteRow($row);
                //return 'del - OK';

            default:
                return 'ERROR';
        }
    }

    public function get($fieldName, $row) {
        // print_r($this->fieldsInverse);
        if (isset($this->fieldsInverse[$fieldName])) {
            $pos = $this->fieldsInverse[$fieldName];
            return $row[$pos];
        } else {
            return null;
        }
    }

    private function addRow($row) {
        $model = new User();
        $report = $this->setUserAttributes($row, $model);
        $model->userEMail = $row[$this->fieldsInverse['userEMail']];
        $model->userLogin = $row[$this->fieldsInverse['userEMail']];

        if ($model->validate() && $model->save()) {
            $report['report'] = 'Success';
            return $report;
        }
        $report['report'] = 'Error';
        return $report;
    }

    private function updateRow($row) {
        $model = User::find()->where(['userEMail' => $row[$this->fieldsInverse['userEMail']]])->one();
        $report = $this->setUserAttributes($row, $model);
        
        if ($model && $model->validate() && $model->save()) {
            $report['report'] = 'Success';
            
            return $report;
        }
        $report['report'] = 'Error';
        
        return $report;
    }

    private function deleteRow($row) {
        $model = User::find()->where(['userEMail' => $row[$this->fieldsInverse['userEMail']]])->one();
        $report = $row;
        if ($model) {
            $report['report'] = 'Success';
            return $report;
        }
        $report['report'] = 'Error';
        return $report;
    }

    /**
     * $userModel = new User()
     */
    private function setUserAttributes($row, $userModel) {
        
        $report=$row;
        // "<b>action</b>",
        // "<b>userEMail</b>",
        // "userPassword",
        if (isset($this->fieldsInverse['userPassword'])) {
            $formula="/^'?=/";
            if (preg_match($formula, $row[$this->fieldsInverse['userPassword']])) {
                $functionName = preg_replace($formula, '', $row[$this->fieldsInverse['userPassword']]);
                $functionName = preg_replace("/\\W+.*\$/", '', $functionName);
                switch ($functionName) {
                    case 'random':
                        $newpassword = substr(base64_encode(random_bytes(20)), 0, 10);
                        break;
                }
            } else {
                $newpassword = $row[$this->fieldsInverse['userPassword']];
            }
            $report[$this->fieldsInverse['userPassword']]=$newpassword;
            $userModel->setPassword($newpassword);
        }

        // userLogin
        //if(isset($this->fieldsInverse['userEMail'])){
        //    $userModel->userLogin=$row[$this->fieldsInverse['userEMail']];
        //}
        // "userFirstName",
        if (isset($this->fieldsInverse['userFirstName'])) {
            $userModel->userFirstName = $row[$this->fieldsInverse['userFirstName']];
        }

        // "userLastName",
        if (isset($this->fieldsInverse['userLastName'])) {
            $userModel->userLastName = $row[$this->fieldsInverse['userLastName']];
        }

        // "userIsValid",
        if (isset($this->fieldsInverse['userIsValid'])) {
            $userModel->userIsValid = $row[$this->fieldsInverse['userIsValid']] ? 1 : 0;
        }

        // "userIsBanned",
        if (isset($this->fieldsInverse['userIsBanned'])) {
            $userModel->userIsBanned = $row[$this->fieldsInverse['userIsBanned']] ? 1 : 0;
        }

        // "userIsActive",
        if (isset($this->fieldsInverse['userIsActive'])) {
            $userModel->userIsActive = $row[$this->fieldsInverse['userIsActive']] ? 1 : 0;
        }

        // "userTelephone",
        if (isset($this->fieldsInverse['userTelephone'])) {
            $userModel->userTelephone = $row[$this->fieldsInverse['userTelephone']];
        }

        // "userLevel"
        if (isset($this->fieldsInverse['userLevel'])) {
            $value = strtolower($row[$this->fieldsInverse['userLevel']]);
            foreach (User::$userLevelOptions as $key => $val) {
                if ($value == strtolower($val)) {
                    $userModel->userLevel = $key;
                    break;
                }
            }
        }
        
        return $report;
    }

}
