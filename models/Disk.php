<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%disk}}".
 *
 * @property string $diskId
 * @property string $diskTitle
 * @property string $diskIcon
 * @property string $diskDescription
 *
 * @property Diskrecord[] $diskrecords
 * @property Groupaccess[] $groupaccesses
 * @property Group[] $groups
 * @property Useraccess[] $useraccesses
 * @property User[] $users
 */
class Disk extends \yii\db\ActiveRecord {

    public $upload_file;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%disk}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['diskDescription'], 'string'],
            [['diskTitle'], 'string', 'max' => 255],
            [['diskIcon'], 'string', 'max' => 1024],
            [['upload_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'diskId' => 'Disk ID',
            'diskTitle' => 'Disk Title',
            'diskIcon' => 'Disk Icon',
            'diskDescription' => 'Disk Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiskrecords() {
        return $this->hasMany(Diskrecord::className(), ['diskId' => 'diskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupaccesses() {
        return $this->hasMany(Groupaccess::className(), ['diskId' => 'diskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups() {
        return $this->hasMany(Group::className(), ['groupId' => 'groupId'])->viaTable('{{%groupaccess}}', ['diskId' => 'diskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseraccesses() {
        return $this->hasMany(Useraccess::className(), ['diskId' => 'diskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['userId' => 'userId'])->viaTable('{{%useraccess}}', ['diskId' => 'diskId']);
    }

    public function uploadFile() {
        // get the uploaded file instance
        $image = UploadedFile::getInstance($this, 'upload_file');

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }
        $uploadDir = Yii::getAlias("@webroot/uploads/{$this->diskId}");
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }
        // generate random name for the file
        $this->diskIcon = "uploads/{$this->diskId}/cover." . $image->extension;

        $image->saveAs(Yii::getAlias("@webroot/").$this->diskIcon);
        // the uploaded image instance
        return $image;
    }
    
    public function getUploadedFile() {
        // return a default image placeholder if your source avatar is not found
        $pic = ( isset($this->diskIcon) && $this->diskIcon ) ? $this->diskIcon : 'uploads/default.png';
        return Yii::getAlias("@web/").$pic;
    }
    
    public function deleteFile() {
        if( isset($this->diskIcon) && $this->diskIcon ){
            $path = Yii::getAlias("@webroot/").$this->diskIcon;
            if(file_exists($path) && is_file($path)){
                unlink($path);
            }
        }
    }
}
