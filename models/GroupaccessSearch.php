<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Groupaccess;

/**
 * GroupaccessSearch represents the model behind the search form about `app\models\Groupaccess`.
 */
class GroupaccessSearch extends Groupaccess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['groupId', 'diskId'], 'integer'],
            [['fromDate', 'toDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Groupaccess::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'groupId' => $this->groupId,
            'diskId' => $this->diskId,
            'fromDate' => $this->fromDate,
            'toDate' => $this->toDate,
        ]);

        return $dataProvider;
    }
}
