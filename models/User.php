<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $userId
 * @property string $userLogin
 * @property string $userPassword
 * @property string $userFirstName
 * @property string $userLastName
 * @property string $userEMail
 * @property integer $userLastAccess
 * @property integer $userAccessDuration
 * @property string $userTemporaryKey
 * @property integer $userIsValid
 * @property integer $userIsBanned
 * @property integer $userIsActive
 * @property string $userTelephone
 * @property integer $userLevel
 * @property string $userToken
 *
 * @property Useraccess[] $useraccesses
 * @property Disk[] $disks
 * @property Usergroup[] $usergroups
 * @property Group[] $groups
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    public static $userLevelOptions = [
        1 => 'Guest',
        2 => 'Student',
        3 => 'Teacher',
        4 => 'Admin'
    ];
    public static $userLevelOptionsInverted = [
        'Guest' => 1,
        'Student' => 2,
        'Teacher' => 3,
        'Admin' => 4
    ];
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userLastAccess', 'userAccessDuration', 'userIsValid', 'userIsBanned', 'userIsActive', 'userLevel'], 'integer'],
            // [['userToken'], 'required'],
            [['userLogin', 'userPassword'], 'string', 'max' => 50],
            [['userFirstName', 'userLastName', 'userEMail', 'userTemporaryKey', 'userTelephone', 'userToken'], 'string', 'max' => 128],
            [['userLogin'], 'unique'],
            [['creatorId'], 'integer'],
            [['creatorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creatorId' => 'userId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'userId' => 'User ID',
            'userLogin' => 'User Login',
            'userPassword' => 'User Password',
            'userFirstName' => 'User First Name',
            'userLastName' => 'User Last Name',
            'userEMail' => 'User Email',
            'userLastAccess' => 'User Last Access',
            'userAccessDuration' => 'User Access Duration',
            'userTemporaryKey' => 'User Temporary Key',
            'userIsValid' => 'User Is Valid',
            'userIsBanned' => 'User Is Banned',
            'userIsActive' => 'User Is Active',
            'userTelephone' => 'User Telephone',
            'userLevel' => 'User Level',
            'userToken' => 'User Token',
            'creatorId'=>'Creator Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseraccesses() {
        return $this->hasMany(Useraccess::className(), ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisks() {
        return $this->hasMany(Disk::className(), ['diskId' => 'diskId'])->viaTable('{{%useraccess}}', ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsergroups() {
        return $this->hasMany(Usergroup::className(), ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups() {
        return $this->hasMany(Group::className(), ['groupId' => 'groupId'])->viaTable('{{%usergroup}}', ['userId' => 'userId']);
    }

    public function getAuthKey() {
        return $this->userToken;
    }
    
    public function getCreator() {
        return $this->hasOne(User::className(), ['userId' => 'creatorId']);
    }

    public function getId() {
        return $this->userId;
    }

    public function validateAuthKey($authKey) {
        return $this->userToken === $authKey;
    }

    public static function findIdentity($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        $user = User::find()->where(['userToken' => $token])->one();
        return $user;
    }

    public function validatePassword($password) {
        $encodedPassword = crypt($password, Yii::$app->params['salt'] . $this->userId);
        // header("x-dbg-001: {$password}+{$this->userId}+{$encodedPassword}+{$this->userPassword}");
        return $encodedPassword == $this->userPassword;
    }

    public function setPassword($newPassword) {
        $this->userPassword = crypt($newPassword, Yii::$app->params['salt'] . $this->userId);
    }

    public static function findByUsername($username) {
        $user = User::find()->where(['userLogin' => $username])->one();
        // var_dump($user);
        return $user;
    }

    /**
     * Yii::$app->user->identity->is($role)
     * $role = admin | teacher | student | guest
     */
    public function is($role) {
        switch (mb_strtolower($role)) {
            case 'admin' : return $this->userLevel == 4;
            case 'teacher': return $this->userLevel == 3;
            case 'student': return $this->userLevel == 2;
            case 'guest' : return $this->userLevel == 1;
        }
    }

    /**
     * \Yii::$app->user->identity->canRead($diskId)
     */
    public function canRead($diskId) {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if ($this->is('admin')) {
            return true;
        }
        if ($this->is('teacher')) {
            return true;
        }
        if (!$this->is('student')) {
            return false;
        }

        // check personal access 
        // SELECT COUNT(*) AS n FROM cd_useraccess WHERE userId=1 AND diskId=1 AND (NOW() BETWEEN fromDate AND toDate)
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
                "SELECT COUNT(*) AS n FROM cd_useraccess WHERE userId=:currentUserId AND diskId=:currentDiskId AND (NOW() BETWEEN fromDate AND toDate)", [':currentUserId' => $this->userId, ':currentDiskId' => $diskId]);
        $result = $command->queryOne();
        if ($result['n'] > 0) {
            return true;
        }


        // check group access
        // SELECT count(*) as n
        // FROM `cd_groupaccess` INNER JOIN `cd_usergroup` ON cd_groupaccess.groupId=cd_usergroup.groupId
        // WHERE (NOW() BETWEEN cd_groupaccess.fromDate AND cd_groupaccess.toDate)
        //   AND cd_groupaccess.diskId=1
        //   AND cd_usergroup.userId=3
        // ;
        $command = $connection->createCommand(
                "SELECT count(*) as n
         FROM `cd_groupaccess` INNER JOIN `cd_usergroup` ON cd_groupaccess.groupId=cd_usergroup.groupId
         WHERE (NOW() BETWEEN cd_groupaccess.fromDate AND cd_groupaccess.toDate)
           AND cd_groupaccess.diskId=:currentDiskId
           AND cd_usergroup.userId=:currentUserId
        ", [':currentUserId' => $this->userId, ':currentDiskId' => $diskId]);
        $result = $command->queryOne();
        if ($result['n'] > 0) {
            return true;
        }
        return false;
    }

    /**
     * \Yii::$app->user->identity->disks()
     */
    public function disks() {
        if (Yii::$app->user->isGuest) {
            return [];
        }

        $ids = [];
        // check personal access 
        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand(
                "SELECT DISTINCT diskId FROM cd_useraccess WHERE userId=:currentUserId AND (NOW() BETWEEN fromDate AND toDate)", [':currentUserId' => $this->userId]);
        $result = $command->queryAll();
        foreach ($result as $r) {
            $ids[] = $r['diskId'];
        }

        // check group access
        $command = $connection->createCommand(
                "SELECT DISTINCT cd_groupaccess.diskId
         FROM `cd_groupaccess` INNER JOIN `cd_usergroup` ON cd_groupaccess.groupId=cd_usergroup.groupId
         WHERE (NOW() BETWEEN cd_groupaccess.fromDate AND cd_groupaccess.toDate)
           AND cd_usergroup.userId=:currentUserId
        ", [':currentUserId' => $this->userId]);
        $result = $command->queryAll();
        foreach ($result as $r) {
            $ids[] = $r['diskId'];
        }
        return $ids;
    }

}
