<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'userLastAccess','creatorId', 'userAccessDuration', 'userIsValid', 'userIsBanned', 'userIsActive', 'userLevel'], 'integer'],
            [['userLogin', 'userPassword', 'userFirstName', 'userLastName', 'userEMail', 'userTemporaryKey', 'userTelephone', 'userToken'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'userId' => $this->userId,
            'userLastAccess' => $this->userLastAccess,
            'userAccessDuration' => $this->userAccessDuration,
            'userIsValid' => $this->userIsValid,
            'userIsBanned' => $this->userIsBanned,
            'userIsActive' => $this->userIsActive,
            'userLevel' => $this->userLevel,
            'creatorId' => $this->creatorId,
        ]);

        $query->andFilterWhere(['like', 'userLogin', $this->userLogin])
            ->andFilterWhere(['like', 'userPassword', $this->userPassword])
            ->andFilterWhere(['like', 'userFirstName', $this->userFirstName])
            ->andFilterWhere(['like', 'userLastName', $this->userLastName])
            ->andFilterWhere(['like', 'userEMail', $this->userEMail])
            ->andFilterWhere(['like', 'userTemporaryKey', $this->userTemporaryKey])
            ->andFilterWhere(['like', 'userTelephone', $this->userTelephone])
            ->andFilterWhere(['like', 'userToken', $this->userToken]);

        return $dataProvider;
    }
}
