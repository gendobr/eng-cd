<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%useraccess}}".
 *
 * @property string $userId
 * @property string $diskId
 * @property string $fromDate
 * @property string $toDate
 *
 * @property User $user
 * @property Disk $disk
 */
class Useraccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%useraccess}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'diskId'], 'required'],
            [['userId', 'diskId'], 'integer'],
            [['fromDate', 'toDate'], 'safe'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'userId']],
            [['diskId'], 'exist', 'skipOnError' => true, 'targetClass' => Disk::className(), 'targetAttribute' => ['diskId' => 'diskId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userId' => 'User ID',
            'diskId' => 'Disk ID',
            'fromDate' => 'From Date',
            'toDate' => 'To Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['userId' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisk()
    {
        return $this->hasOne(Disk::className(), ['diskId' => 'diskId']);
    }
}
