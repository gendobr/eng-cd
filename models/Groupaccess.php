<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%groupaccess}}".
 *
 * @property string $groupId
 * @property string $diskId
 * @property string $fromDate
 * @property string $toDate
 *
 * @property Group $group
 * @property Disk $disk
 */
class Groupaccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%groupaccess}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['groupId', 'diskId'], 'required'],
            [['groupId', 'diskId'], 'integer'],
            [['fromDate', 'toDate'], 'safe'],
            [['groupId'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['groupId' => 'groupId']],
            [['diskId'], 'exist', 'skipOnError' => true, 'targetClass' => Disk::className(), 'targetAttribute' => ['diskId' => 'diskId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'groupId' => 'Group ID',
            'diskId' => 'Disk ID',
            'fromDate' => 'From Date',
            'toDate' => 'To Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['groupId' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisk()
    {
        return $this->hasOne(Disk::className(), ['diskId' => 'diskId']);
    }
}
