<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%group}}".
 *
 * @property string $groupId
 * @property string $groupName
 * @property string $groupDescription
 *
 * @property Groupaccess[] $groupaccesses
 * @property Disk[] $disks
 * @property Usergroup[] $usergroups
 * @property User[] $users
 */
class Group extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['groupDescription'], 'string'],
            [['groupName'], 'string', 'max' => 255],
            [['creatorId'], 'integer'],
            [['creatorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creatorId' => 'userId']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator() {
        return $this->hasOne(User::className(), ['userId' => 'creatorId']);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'groupId' => 'Group ID',
            'groupName' => 'Group Name',
            'creatorId'=>'Creator Id',
            'groupDescription' => 'Group Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupaccesses() {
        return $this->hasMany(Groupaccess::className(), ['groupId' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisks() {
        return $this->hasMany(Disk::className(), ['diskId' => 'diskId'])->viaTable('{{%groupaccess}}', ['groupId' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsergroups() {
        return $this->hasMany(Usergroup::className(), ['groupId' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(User::className(), ['userId' => 'userId'])->viaTable('{{%usergroup}}', ['groupId' => 'groupId']);
    }

}
