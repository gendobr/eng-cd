<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%diskrecord}}".
 *
 * @property string $diskrecordId
 * @property string $diskrecordFilePathMp3
 * @property string $diskrecordFilePathWav
 * @property string $diskrecordFilePathOgg
 * @property string $diskrecordTitle
 * @property string $diskId
 * @property integer $diskrecordOrdering
 *
 * @property Disk $disk
 */
class Diskrecord extends \yii\db\ActiveRecord {

    public $uploadMp3;
    public $uploadOgg;
    public $uploadWav;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%diskrecord}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['diskId'], 'required'],
            [['diskId', 'diskrecordOrdering'], 'integer'],
            [['diskrecordFilePathMp3', 'diskrecordFilePathOgg', 'diskrecordFilePathWav', 'diskrecordTitle'], 'string', 'max' => 1024],
            [['diskId'], 'exist', 'skipOnError' => true, 'targetClass' => Disk::className(), 'targetAttribute' => ['diskId' => 'diskId']],
            [['uploadMp3'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp3'],
            [['uploadOgg'], 'file', 'skipOnEmpty' => true, 'extensions' => 'ogg'],
            [['uploadWav'], 'file', 'skipOnEmpty' => true, 'extensions' => 'wav'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'diskrecordId' => 'Diskrecord ID',
            'diskrecordFilePathMp3' => 'Diskrecord Mp3 File Path',
            'diskrecordFilePathOgg' => 'Diskrecord Ogg File Path',
            'diskrecordFilePathWav' => 'Diskrecord Wav File Path',
            'diskrecordTitle' => 'Diskrecord Title',
            'diskId' => 'Disk ID',
            'diskrecordOrdering' => 'Diskrecord Ordering',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisk() {
        return $this->hasOne(Disk::className(), ['diskId' => 'diskId']);
    }

    public function uploadMp3() {
        // if no image was uploaded abort the upload
        if (!isset($_FILES['uploadMp3'])) {
            return false;
        }
        $uploadDir = Yii::getAlias("@webroot/uploads/{$this->diskId}");
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }
        // generate random name for the file
        $this->diskrecordFilePathMp3 = "uploads/{$this->diskId}/track.{$this->diskrecordId}." . preg_replace('/[^a-z0-9_.-]/i', '-', $_FILES["uploadMp3"]["name"]);

        $file = move_uploaded_file($_FILES["uploadMp3"]["tmp_name"], Yii::getAlias("@webroot/") . $this->diskrecordFilePathMp3);
        // the uploaded image instance
        return $file;
    }

    public function getUploadedMp3() {
        return Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$this->diskrecordId}&file=mp3";
    }

    public function deleteMp3() {
        if (isset($this->diskrecordFilePathMp3) && $this->diskrecordFilePathMp3) {
            $path = Yii::getAlias("@webroot/") . $this->diskrecordFilePathMp3;
            if (file_exists($path) && is_file($path)) {
                unlink($path);
            }
        }
    }

    public function uploadOgg() {
        // if no image was uploaded abort the upload
        if (!isset($_FILES['uploadOgg'])) {
            return false;
        }
        $uploadDir = Yii::getAlias("@webroot/uploads/{$this->diskId}");
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }
        // generate random name for the file
        $this->diskrecordFilePathOgg = "uploads/{$this->diskId}/track.{$this->diskrecordId}." . preg_replace('/[^a-z0-9_.-]/i', '-', $_FILES["uploadOgg"]["name"]);

        $file = move_uploaded_file($_FILES["uploadOgg"]["tmp_name"], Yii::getAlias("@webroot/") . $this->diskrecordFilePathOgg);
        // the uploaded image instance
        return $file;
    }

    public function getUploadedOgg() {
        return Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$this->diskrecordId}&file=ogg";
    }

    public function deleteOgg() {
        if (isset($this->diskrecordFilePathOgg) && $this->diskrecordFilePathOgg) {
            $path = Yii::getAlias("@webroot/") . $this->diskrecordFilePathOgg;
            if (file_exists($path) && is_file($path)) {
                unlink($path);
            }
        }
    }

    public function uploadWav() {
        // if no image was uploaded abort the upload
        if (!isset($_FILES['uploadWav'])) {
            return false;
        }
        $uploadDir = Yii::getAlias("@webroot/uploads/{$this->diskId}");
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }
        // generate random name for the file
        $this->diskrecordFilePathWav = "uploads/{$this->diskId}/track.{$this->diskrecordId}." . preg_replace('/[^a-z0-9_.-]/i', '-', $_FILES["uploadWav"]["name"]);

        $file = move_uploaded_file($_FILES["uploadWav"]["tmp_name"], Yii::getAlias("@webroot/") . $this->diskrecordFilePathWav);
        // the uploaded image instance
        return $file;
    }

    public function getUploadedWav() {
        return Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$this->diskrecordId}&file=wav";
    }

    public function deleteWav() {
        if (isset($this->diskrecordFilePathWav) && $this->diskrecordFilePathWav) {
            $path = Yii::getAlias("@webroot/") . $this->diskrecordFilePathWav;
            if (file_exists($path) && is_file($path)) {
                unlink($path);
            }
        }
    }

}
