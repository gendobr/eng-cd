<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Disk;

/**
 * DiskSearch represents the model behind the search form about `app\models\Disk`.
 */
class DiskSearch extends Disk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['diskId'], 'integer'],
            [['diskTitle', 'diskIcon', 'diskDescription'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Disk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'diskId' => $this->diskId,
        ]);

        $query->andFilterWhere(['like', 'diskTitle', $this->diskTitle])
            ->andFilterWhere(['like', 'diskIcon', $this->diskIcon])
            ->andFilterWhere(['like', 'diskDescription', $this->diskDescription]);

        return $dataProvider;
    }
}
