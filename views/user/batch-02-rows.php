<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use kartik\widgets\Select2;


$this->title = 'Batch User Operations';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (isset($message) && strlen($message) > 0) {
        ?>
        <div class="alert alert-info"><?= $message ?></div>
        <?php
    }
    ?>
    <div class="user-form">
        <?php $form = ActiveForm::begin(); ?>
        
        <?= Html::hiddenInput ( 'fields', json_encode($model->fields) )?>
        <?= Html::hiddenInput ( 'rows', json_encode($model->rows) )?>
        
        <!-- draw table -->
        <h4>Operations to execute:</h4>
        <table class='table table-striped table-bordered'>
            <tr>
                <?php
                foreach($model->fields as $fld){
                    echo "<th>{$fld}</th>";
                }
                ?>
            </tr>
            <tr>
                <?php
                foreach($model->rows as $row){
                    echo "<tr>";
                    foreach($row as $cell){
                        echo "<td>{$cell}</td>";
                    }
                    echo "</tr>";
                }
                ?>
            </tr>
        </table>
        
        <div class="form-group">
            <?= Html::submitButton('Confirm') ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>