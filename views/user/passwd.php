<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use kartik\widgets\Select2;


$this->title = 'Password Recovery';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (isset($message) && strlen($message) > 0) {
        ?>
        <div class="alert alert-info"><?= $message ?></div>
        <?php
    }
    ?>
    <div class="user-form">

        <?php $form = ActiveForm::begin(['action' =>['user/passwd', 'key'=>$key],  'method' => 'post',]); ?>

        <h3>User: <?=$model->userLogin?></h3>

        <div class="form-group field-user-password">
            <label class="control-label" for="user-password">Password</label>
            <input id="user-password" class="form-control" name="password[0]" value="" maxlength="128" type="password">
        </div>
        <div class="form-group field-user-password">
            <label class="control-label" for="user-password">Re-type password</label>
            <input id="user-password" class="form-control" name="password[1]" value="" maxlength="128" type="password">
        </div>

        <div class="form-group">
            <?= Html::submitButton('Save new password', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>