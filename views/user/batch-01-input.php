<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use kartik\widgets\Select2;


$this->title = 'Batch User Operations';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (isset($message) && strlen($message) > 0) {
        ?>
        <div class="alert alert-info"><?= $message ?></div>
        <?php
    }
    ?>
    <div class="user-form">
        <?php $form = ActiveForm::begin(); ?>
        
        <div class='row'>
            <div class='col-sm-4'><?= $form->field($model, 'delimiter')->textInput(['name'=>'delimiter']) ?></div>
            <div class='col-sm-4'><?= $form->field($model, 'enclosure')->textInput(['name'=>'enclosure']) ?></div>
            <div class='col-sm-4'><?= $form->field($model, 'escape')->textInput(['name'=>'escape']) ?></div>
        </div>

        <?= $form->field($model, 'csv')->textarea(['rows' => '16','name'=>'csv']) ?>
        <div class="form-group">
            <?= Html::submitButton('Submit') ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
        
<h4>Sample data</h4>
<b style='color:red;'>Bold columns are mandatory</b>
<pre style='border:1px solid green;padding:10px;'>
"<b>action</b>","<b>userEMail</b>","userPassword","userFirstName","userLastName","userIsValid","userIsBanned","userIsActive","userTelephone","userLevel"
"<b>add</b>","<b>test1@mail.com</b>","=random()","Some First Name","Some Last Name","1","0","1","+3801234567890","admin"
"<b>update</b>","<b>test3@mail.com</b>","akdkwo76","Another First Name","Another Last Name","1","0","1","+3801234567890","teacher"
"<b>update</b>","<b>test4@mail.com</b>","kki876tg","More First Name","More Last Name","1","0","1","+3801234567890","student"
"<b>update</b>","<b>test4@mail.com</b>","e2itr876","Next First Name","Next Last Name","0","1","0","+3801234567890","guest"
"<b>delete</b>","<b>test2@mail.com</b>","","","","","","","",""
</pre>
        
</div>