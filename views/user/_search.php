<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'userLogin') ?>

    <?= $form->field($model, 'userPassword') ?>

    <?= $form->field($model, 'userFirstName') ?>

    <?= $form->field($model, 'userLastName') ?>

    <?php // echo $form->field($model, 'userEMail') ?>

    <?php // echo $form->field($model, 'userLastAccess') ?>

    <?php // echo $form->field($model, 'userAccessDuration') ?>

    <?php // echo $form->field($model, 'userTemporaryKey') ?>

    <?php // echo $form->field($model, 'userIsValid') ?>

    <?php // echo $form->field($model, 'userIsBanned') ?>

    <?php // echo $form->field($model, 'userIsActive') ?>

    <?php // echo $form->field($model, 'userTelephone') ?>

    <?php // echo $form->field($model, 'userLevel') ?>

    <?php // echo $form->field($model, 'userToken') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
