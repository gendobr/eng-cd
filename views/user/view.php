<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->userLogin;

if (\Yii::$app->user->identity->is('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
} elseif (\Yii::$app->user->identity->is('teacher')) {
    $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['teacherindex']];
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?>


        <?= Html::a('Update', ['update', 'id' => $model->userId], ['class' => 'btn btn-primary btn-xs']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->userId], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </h1>
    <?php
    $attributes = [
        'userId',
        'userLogin',
        //'userPassword',
        'userFirstName',
        'userLastName',
        'userEMail:email',
        [
            'attribute' => 'userLastAccess',
            'label' => 'Last Access',
            'value' => date('r', $model->userLastAccess),
        ],
        // 'userAccessDuration',
        // 'userTemporaryKey',
        'userIsValid',
        // 'userIsBanned',
        // 'userIsActive',
        'userTelephone',
        //'userLevel',
        [
            'attribute' => 'userLevel',
            'label' => 'Access Level',
            'value' => (isset(app\models\User::$userLevelOptions[$model->userLevel]) ? app\models\User::$userLevelOptions[$model->userLevel] : ''),
        ],
            // 'userToken',
    ];
    if (\Yii::$app->user->identity->is('admin')) {
        $attributes[] = [
            'attribute' => 'creatorId',
            'label' => 'Creator',
            'format' => 'raw',
            'value' => ( ($md = $model->creator) ? Html::a("{$md->userLastName} {$md->userFirstName}, {$md->userLogin}", ['view', 'id' => $md->userId], ['class' => '']) : '' )
        ];
    }
    ?>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ])
    ?>

</div>
