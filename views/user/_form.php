<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->errorSummary($model); ?>
    
    <?= $form->field($model, 'userLogin')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'userFirstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userLastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userEMail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userTelephone')->textInput(['maxlength' => true]) ?>

    
    <?php
    if(\Yii::$app->user->identity->is('teacher')){
        echo $form->field($model, 'userLevel')->hiddenInput(['value'=> app\models\User::$userLevelOptionsInverted['Student']])->label(false);
    }elseif(\Yii::$app->user->identity->is('admin')){
        echo $form->field($model, 'userLevel')->widget(Select2::classname(), [
            'data' => app\models\User::$userLevelOptions,
            'showToggleAll'=>false,
            'options' => ['placeholder' => 'Select Access Level ...', 'multiple' => false],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        
    }
    
    
    ?>

    <?= $form->field($model, 'userIsValid')->checkbox() ?>

    <?= $form->field($model, 'userIsBanned')->checkbox() ?>

    <h3>New password</h3>
    <div class="form-group field-user-password">
    <label class="control-label" for="user-password">Password</label>
    <input id="user-password" class="form-control" name="password[0]" value="" maxlength="128" type="password">
    </div>
    <div class="form-group field-user-password">
    <label class="control-label" for="user-password">Re-type password</label>
    <input id="user-password" class="form-control" name="password[1]" value="" maxlength="128" type="password">
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
