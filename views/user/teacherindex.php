<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?>
    <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success btn-xs']) ?>
    </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'userId',
            'userLogin',
            //'userPassword',
            'userFirstName',
            'userLastName',
            'userEMail:email',
            [
                'attribute' => 'userLastAccess',
                'label'=>'Last Access',
                'content' => function ($model, $key, $index, $column) {
                    return date('r',$model['userLastAccess']);
                }
            ],
            // 'userAccessDuration',
            // 'userTemporaryKey',
            // 'userIsValid',
            // 'userIsBanned',
            // 'userIsActive',
            'userTelephone',
            //[
            //    'attribute' => 'userLevel',
            //    'label'=>'Acces Level',
            //    'filter' => app\models\User::$userLevelOptions,
            //    'content' => function ($model, $key, $index, $column) {
            //        return isset(app\models\User::$userLevelOptions[$model['userLevel']]) ? app\models\User::$userLevelOptions[$model['userLevel']] : '';
            //    }
            //],
            // 'userToken',

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
