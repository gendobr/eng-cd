<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use kartik\widgets\Select2;


$this->title = 'Password Recovery';

?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        if(strlen($message)>0){
            ?>
            <div class="alert alert-info"><?=$message?></div>
            <?php
        }
    ?>
    <div class="user-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'userEMail')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
        <?= Html::submitButton( 'Send me my new password', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>