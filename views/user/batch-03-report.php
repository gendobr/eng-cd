<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// use kartik\widgets\Select2;


$this->title = 'Batch User Operations';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (isset($message) && strlen($message) > 0) {
        ?>
        <div class="alert alert-info"><?= $message ?></div>
        <?php
    }
    ?>
    <div class="user-form">
        <!-- draw table -->
        <h4>Operations to execute:</h4>
        <table class='table table-striped table-bordered'>
            <tr>
                <th>Report</th>
                <?php
                foreach ($model->fields as $fld) {
                    echo "<th>{$fld}</th>";
                }
                ?>
            </tr>
            <tr>
                <?php
                // print_r($report);exit();
                foreach ($report as $rowId => $row) {
                    
                    echo "<tr>";
                    echo "<td>{$row['report']}</td>";
                    foreach ($model->fields as $fld) {
                        $cell = $model->get($fld, $row);
                        echo "<td>{$cell}</td>";
                    }
                    echo "</tr>";
                }
                ?>
            </tr>
        </table>



        <!--  -->        
        <?php
        $toCSV = function($rows, $delimiter = ',', $enclosure = '"', $escape_char = "\\" ) {
            // Open a memory "file" for read/write...
            $fp = fopen('php://temp', 'r+');
            // ... write the $input array to the "file" using fputcsv()...
            foreach ($rows as $input) {
                // print_r($input);
                fputcsv($fp, $input, $delimiter, $enclosure, $escape_char);
            }
            // ... rewind the "file" so we can read what we just wrote...
            rewind($fp);
            // ... read the entire line into a variable...
            $data = [];
            while (($buffer = fgets($fp)) !== false) {
                $data[] = $buffer;
            }

            // ... close the "file"...
            fclose($fp);
            // ... and return the $data to the caller, with the trailing newline from fgets() removed.
            // return rtrim($data, "\n");
            return join("",$data);
        };
        $toCorrect = [];
        $toCorrect[] = $model->fields;
        foreach ($report as $rowId => $row) {
            if ($row['report'] == 'Error') {
                $toCorrect[] = $model->rows[$rowId];
            }
        }
        // print_r($toCorrect);
        ?>
        <h3>The Chance</h3>
        You can correct the errors and submit the form again.
        <?php $form = ActiveForm::begin(); ?>


        <div class='row'>
            <div class='col-sm-4'><?= $form->field($model, 'delimiter')->textInput(['name' => 'delimiter']) ?></div>
            <div class='col-sm-4'><?= $form->field($model, 'enclosure')->textInput(['name' => 'enclosure']) ?></div>
            <div class='col-sm-4'><?= $form->field($model, 'escape')->textInput(['name' => 'escape']) ?></div>
        </div>

        <div class="form-group field-userbatchform-csv">
            <label class="control-label" for="userbatchform-csv">Csv</label>
            <?= Html::textarea('csv', $toCSV($toCorrect), ["id" => "userbatchform-csv", "class" => "form-control", "rows" => "16"]) ?>

            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Submit') ?>
        </div>

        <?php ActiveForm::end();
        ?>
    </div>
</div>