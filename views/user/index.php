<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?>
    <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success btn-xs']) ?>
    </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?php Pjax::begin(); ?>    <?php
    $columns = [];

    //['class' => 'yii\grid\SerialColumn'],
    $columns[] = ['class' => 'yii\grid\ActionColumn'];

    $columns[] = 'userId';
    $columns[] = 'userLogin';
    //'userPassword',
    $columns[] = 'userFirstName';
    $columns[] = 'userLastName';
    $columns[] = 'userEMail:email';
    $columns[] = [
        'attribute' => 'userLastAccess',
        'label' => 'Last Access',
        'content' => function ($model, $key, $index, $column) {
            return date('r', $model['userLastAccess']);
        }
    ];
    // 'userAccessDuration',
    // 'userTemporaryKey',
    // 'userIsValid',
    // 'userIsBanned',
    // 'userIsActive',
    $columns[] = 'userTelephone';

    if (\Yii::$app->user->identity->is('admin')) {
        $columns[] = [
            'attribute' => 'userLevel',
            'label' => 'Acces Level',
            'filter' => app\models\User::$userLevelOptions,
            'content' => function ($model, $key, $index, $column) {
                return isset(app\models\User::$userLevelOptions[$model['userLevel']]) ? app\models\User::$userLevelOptions[$model['userLevel']] : '';
            }
        ];

        $creatorOptionsQuery = User::find()->where(['or','userLevel='.(User::$userLevelOptionsInverted['Teacher']),'userLevel='.(User::$userLevelOptionsInverted['Admin'])])->all();
        $creatorOptions=[];
        foreach($creatorOptionsQuery as $md){
            $creatorOptions[$md->userId]="{$md->userLastName} {$md->userFirstName}, {$md->userLogin}";
        }
        
        
        // print_r($creatorOptions);
        
        $columns[] = [
            'attribute' => 'creatorId',
            'label' => 'Creator Id',
            'filter' => $creatorOptions,
            'content' => function ($model, $key, $index, $column) {
                $creator = $model->creator;
                return $creator ? "{$creator->userLastName} {$creator->userFirstName}, {$creator->userLogin}" : '';
            }
        ];
    }



    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]);
    ?>
    <?php Pjax::end(); ?></div>
