<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Create User';
if(\Yii::$app->user->identity->is('admin')){
    $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
}elseif(\Yii::$app->user->identity->is('teacher')){
    $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['teacherindex']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
