<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['sitename'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    
    
    $nav_items=[];
    // $nav_items[]=['label' => 'Home', 'url' => ['/site/index']];
    // $nav_items[]=['label' => 'About', 'url' => ['/site/about']];
    if(Yii::$app->user->isGuest){
        $nav_items[]= ['label' => 'Login', 'url' => ['/site/login']]   ;
    }else{

	$nav_items[]=['label' => 'Техническая поддержка', 'url' => 'https://docs.google.com/forms/d/e/1FAIpQLSfLIyknsWduuBQgl066DopDNrRsMl3rUXaOVyz_2fyw1khQeQ/viewform'];

        // if is admin
        if(Yii::$app->user->identity->is("admin")){
            $nav_items[]=['label' => 'Admin', 
                'items' => [
                    ['label' => 'Users', 'url' => ['/user/index']],
                    ['label' => 'User Batch Operation', 'url' => ['/user/batch']],
                    ['label' => 'Disks', 'url' => ['/disk/index']],
                    ['label' => 'Groups', 'url' => ['/group/index']],
                ]
            ];
        }
        
        // if is teacher
        if(Yii::$app->user->identity->is("teacher")){
            $nav_items[]=['label' => 'Teacher', 
                'items' => [
                    ['label' => 'Users', 'url' => ['/user/teacherindex']],
                    ['label' => 'Disks', 'url' => ['/disk/teacherindex']],
                    ['label' => 'Groups', 'url' => ['/group/teacherindex']],
                ]
            ];
        }
        
        // if is student
        if(Yii::$app->user->identity->is("student")){
            $nav_items[]= ['label' => 'Disks', 'url' => ['/disk/studentindex']];
        }
        
        $nav_items[]= '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->userLogin . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $nav_items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=Yii::$app->params['sitename']?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
