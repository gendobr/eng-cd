<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a('Create Group', ['create'], ['class' => 'btn btn-success btn-xs']) ?></h1>

<?php Pjax::begin(); ?>    <?php

$columns = [];

$columns[]=['class' => 'yii\grid\ActionColumn'];
$columns[]='groupId';
$columns[]='groupName';
$columns[]='groupDescription:ntext';

// echo "++++++++++++++++";
if (\Yii::$app->user->identity->is('admin')) {
    
        $creatorOptionsQuery = User::find()->where(['or','userLevel='.(User::$userLevelOptionsInverted['Teacher']),'userLevel='.(User::$userLevelOptionsInverted['Admin'])])->all();
        $creatorOptions=[];
        foreach($creatorOptionsQuery as $md){
            $creatorOptions[$md->userId]="{$md->userLastName} {$md->userFirstName}, {$md->userLogin}";
        }
        //        var_dump(join(',',array_keys($creatorOptions)));
        //        echo '<hr>';
        //        var_dump($creatorOptions);
        $columns[] = [
            'attribute' => 'creatorId',
            'label' => 'Creator',
            'filter' => $creatorOptions,
            'content' => function ($model, $key, $index, $column) {
                $creator = $model->creator;
                return $creator ? "{$creator->userLastName} {$creator->userFirstName}, {$creator->userLogin}" : '';
            }
        ];
}

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
<?php Pjax::end(); ?></div>
