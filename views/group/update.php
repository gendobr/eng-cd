<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = 'Update Group: ' . $model->groupName;

if(\Yii::$app->user->identity->is('admin')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
}elseif(\Yii::$app->user->identity->is('teacher')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['teacherindex']];
}

$this->params['breadcrumbs'][] = ['label' => $model->groupId, 'url' => ['view', 'id' => $model->groupId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

<br/><br/>

<h3>Group Members</h3>
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
            <th style="width:10%;"></th>
            <th style="width:90%;">Name</th>
        </tr>
    </thead>
    <tbody id="usercontainer">
    </tbody>
</table>


<h3>Add User</h3>
<div class="row">
    <div class="col-sm-4">Add user:<br/>
        <?php
        // echo '<label class="control-label">Provinces</label>';
        echo Select2::widget([
            'name' => 'newUserId',
            'data' => ArrayHelper::map($students, 'userId', 'userFullName'),
            'options' => [
                'placeholder' => 'Select students ...',
                'multiple' => true,
                'id' => 'newUserId'
            ],
        ]);
        ?>

    </div>
</div>
<br/>
<button id="btnAddUser" class="btn btn-primary"> Add </button>




<br/><br/>
<h3>Disks Access</h3>
<table class='table table-striped table-bordered'>
    <thead>
        <tr>
            <th style="width:10%;"></th>
            <th style="width:50%;">Disk</th>
            <th style="width:20%;">From Date</th>
            <th style="width:20%;">To Date</th>
        </tr>
    </thead>
    <tbody id="container">
    </tbody>
</table>

<h3>Add/Replace Access</h3>
<div class="row">
    <div class="col-sm-4">Disk:<br/>
        <?php echo Html::dropDownList('list', '', ArrayHelper::map($disks, 'diskId', 'diskTitle'), ['class' => 'form-control', 'id' => 'newdiskId']); ?>
    </div>
    <div class="col-sm-2">From Date:<br/><input type="text" id="fromDate" class="form-control"></div>
    <div class="col-sm-2">To Date:<br/><input type="text" id="toDate" class="form-control"></div>
</div>
<br/>
<button id="btnAdd" class="btn btn-primary"> Add / Replace </button>

<?php
$this->registerJsFile( '@web/js/groupaccess.js', ['depends' => [\yii\web\JqueryAsset::className()]] );
$this->registerJsFile( '@web/js/usergroup.js', ['depends' => [\yii\web\JqueryAsset::className()]] );

$this->registerJsFile(
        '@web/js/datepicker/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs(
        "   
                window.groupaccess.init({
                    groupId:{$model->groupId},
                    containerId: 'container',
                    btnAdd:'btnAdd'
                });

                jQuery('#fromDate').datepicker({format: 'yyyy-mm-dd'});
                jQuery('#toDate').datepicker({format: 'yyyy-mm-dd'});

                window.usergroup.init({
                    groupId:{$model->groupId},
                    containerId: 'usercontainer',
                    btnAdd:'btnAddUser'
                });

               ", yii\web\View::POS_LOAD, 'my-groupaccess-handler'
);

$this->registerCssFile('@web/js/datepicker/css/bootstrap-datepicker.min.css');

$this->registerCss(
        "   
                .del, .save{ cursor:pointer; margin-right:10px;}
            ");
?>
