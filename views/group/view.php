<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = $model->groupName; //.' ('.$model->groupId.')';

if(\Yii::$app->user->identity->is('admin')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
}elseif(\Yii::$app->user->identity->is('teacher')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['teacherindex']];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Update', ['update', 'id' => $model->groupId], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->groupId], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </h1>
    <div><?php
    if(\Yii::$app->user->identity->is('admin')){
        $creator = $model->creator;
        if($creator){
            echo "Creator : ". Html::a(" {$creator->userLastName} {$creator->userFirstName}, {$creator->userLogin}", ['/user/view', 'id' => $model->creatorId], ['class' => '']);
        }
    }
    ?></div>
    <div>
        <?=$model->groupDescription?>
    </div>
    
    <h3>Members</h3>
    <div>
        <ol>
        <?php
        foreach($users as $user){
            echo "<li>{$user['userLastName']}, {$user['userFirstName']}, {$user['userLogin']}, {$user['userEMail']}</li>";
        }
        ?>
        </ol>
    </div>

    <h3>Disks</h3>
    <div>
        <ol>
        <?php
        foreach($disks as $disk){
            $fromDate=strtotime($disk['fromDate']);
            $toDate=strtotime($disk['toDate']);

            echo "<li> <span style='color:gray;'>".date(Yii::$app->params['dateformat'],$fromDate)." - ".date(Yii::$app->params['dateformat'],$toDate)."</span>  {$disk['diskTitle']} </li>";
        }
        ?>
        </ol>
    </div>
</div>
