<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = 'Create Group';

if(\Yii::$app->user->identity->is('admin')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
}elseif(\Yii::$app->user->identity->is('teacher')){
    $this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['teacherindex']];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
