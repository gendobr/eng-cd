<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usergroup */

$this->title = 'Update Usergroup: ' . $model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Usergroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userId, 'url' => ['view', 'userId' => $model->userId, 'groupId' => $model->groupId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usergroup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
