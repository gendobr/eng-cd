<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Groupaccess */

$this->title = 'Update Groupaccess: ' . $model->groupId;
$this->params['breadcrumbs'][] = ['label' => 'Groupaccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->groupId, 'url' => ['view', 'groupId' => $model->groupId, 'diskId' => $model->diskId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="groupaccess-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
