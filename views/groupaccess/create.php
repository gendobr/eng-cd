<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Groupaccess */

$this->title = 'Create Groupaccess';
$this->params['breadcrumbs'][] = ['label' => 'Groupaccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groupaccess-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
