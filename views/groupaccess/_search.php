<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GroupaccessSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groupaccess-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'groupId') ?>

    <?= $form->field($model, 'diskId') ?>

    <?= $form->field($model, 'fromDate') ?>

    <?= $form->field($model, 'toDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
