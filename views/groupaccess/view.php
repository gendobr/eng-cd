<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Groupaccess */

$this->title = $model->groupId;
$this->params['breadcrumbs'][] = ['label' => 'Groupaccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groupaccess-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'groupId' => $model->groupId, 'diskId' => $model->diskId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'groupId' => $model->groupId, 'diskId' => $model->diskId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'groupId',
            'diskId',
            'fromDate',
            'toDate',
        ],
    ]) ?>

</div>
