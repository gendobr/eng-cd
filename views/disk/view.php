<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Disk */

$this->title = $model->diskTitle;
if (\Yii::$app->user->identity->is('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['index']];
}
if (\Yii::$app->user->identity->is('teacher')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['teacherindex']];
}
if (\Yii::$app->user->identity->is('student')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['studentindex']];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disk-view">



    <div class="row">
        <div class="col-xs-12 col-sm-6 diskIcon">
            <img src="<?= $model->diskIcon ?>">
        </div>
        <div class="col-xs-12 col-sm-6 diskContent">
            <h1><?php
                echo Html::encode($this->title);
                if (\Yii::$app->user->identity->is('admin')) {
                    echo ' ' . Html::a('Update', ['update', 'id' => $model->diskId], ['class' => 'btn btn-primary btn-xs']);
                    echo ' ' . Html::a('Access', ['access', 'id' => $model->diskId], ['class' => 'btn btn-primary btn-xs']);
                    echo ' ' . Html::a('Delete', ['delete', 'id' => $model->diskId], [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                }
                if (\Yii::$app->user->identity->is('teacher')) {
                    echo ' ' . Html::a('Access', ['access', 'id' => $model->diskId], ['class' => 'btn btn-primary btn-xs']);
                }
                ?></h1>
            <div>
                <?= $model->diskDescription ?>
            </div>
            <p>&nbsp;</p>
            <div class="tracks">
                <div id="jp_container_N" class="jp-video jp-video-270p" role="application" aria-label="media player">
                    <div class="jp-type-playlist">
                        <div id="jquery_jplayer_N" class="jp-jplayer"></div>
                        <div class="jp-gui">
                            <div class="jp-video-play">
                                <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                            </div>
                            <div class="jp-interface">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                <div class="jp-controls-holder">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0">previous</button>
                                        <button class="jp-play" role="button" tabindex="0">play</button>
                                        <button class="jp-next" role="button" tabindex="0">next</button>
                                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <button class="jp-mute" role="button" tabindex="0">mute</button>
                                        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-toggles">
                                        <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                        <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                        <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                    </div>
                                </div>
                                <div class="jp-details">
                                    <div class="jp-title" aria-label="title">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="jp-playlist">
                            <ul>
                                <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                        <div class="jp-no-solution">
                            <span>Update Required</span>
                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<style type="text/css">
    .diskIcon img{
        max-width:100%;
    }
    .disklistItem h3{
        text-align:left;
        white-space:nowrap;
        width:100%;
        overflow:hidden;
    }
</style>
<?php
// create list of tracks
$playlist = [];
// echo 'diskrecords ';
// print_r($diskrecords);
foreach ($diskrecords as $dr) {
    $playlist[]=[
	'title'=>$dr['diskrecordTitle'],
	'artist'=>"Gaudeamus.ua",
        'mp3'=>Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$dr['diskrecordId']}&file=mp3",
	'oga'=>Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$dr['diskrecordId']}&file=ogg",
	'wav'=>Yii::getAlias("@web/") . "index.php?r=diskrecord/stream&diskrecordId={$dr['diskrecordId']}&file=wav",
    ];
}

$this->registerJsFile('@web/js/jplayer/dist/jplayer/jquery.jplayer.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/jplayer/dist/add-on/jplayer.playlist.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('@web/js/jplayer/dist/skin/blue.monday/css/jplayer.blue.monday.min.css');
$this->registerJs(
'   
var myPlaylist = new jPlayerPlaylist({
		jPlayer: "#jquery_jplayer_N",
		cssSelectorAncestor: "#jp_container_N"
	}, '.
        json_encode($playlist)
        .', {
		playlistOptions: {
			enableRemoveControls: true
		},
		swfPath: "js/jplayer/dist/jplayer",
		supplied: "wav, oga, mp3",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		audioFullScreen: true
	});

                    
', yii\web\View::POS_LOAD, 'my-jplayer-init'
);

//    $this->registerCss(
//            "   
//              .tracks  div.jp-type-playlist div.jp-playlist a.jp-playlist-item-remove{ display:none;}
//            ");
?>

