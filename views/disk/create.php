<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Disk */

$this->title = 'Create Disk';
if (\Yii::$app->user->identity->is('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['index']];
}
if (\Yii::$app->user->identity->is('teacher')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['teacherindex']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
