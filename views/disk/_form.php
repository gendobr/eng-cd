<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Disk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disk-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'diskTitle')->textInput(['maxlength' => true]) ?>

    <img src="<?=$model->diskIcon?>">
    <?= $form->field($model, 'upload_file')->fileInput() ?>

    <?= $form->field($model, 'diskDescription')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
