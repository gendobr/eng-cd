<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DiskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disk-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Create Disk', ['create'], ['class' => 'btn btn-xs btn-success']) ?>
    </h1>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',
  
             'template' => '{view} {update} {access} &nbsp;&nbsp;&nbsp;&nbsp;{delete}',
             'buttons' => [ 
                    'access' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', 
                                      ['access', 'id' => $model->diskId], 
                                      ['title' => "Access"]);
                    },        
                    'view'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 
                                ['view', 'id' => $model->diskId], 
                                ['title' => "View"]);
                    },
                    'update'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 
                                ['update', 'id' => $model->diskId], 
                                ['title' => "Edit"]);
                    },
                    'delete'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
                                ['delete', 'id' => $model->diskId], 
                                ['title' => "Delete","data-method"=>"post"]);
                    },
                ]

                ],

            // 'diskId',
            'diskTitle',
            // 'diskIcon',
            // 'diskDescription:ntext',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
