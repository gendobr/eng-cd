<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Available Disks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
    <?php 
    foreach($disks as $disk){
        echo '<div class="disklistItem col-xs-12 col-sm-4 col-md-3">';
        echo Html::a("&nbsp;", ['view', 'id' => $disk->diskId],  ['title' => "View", "class"=>"disklistItemIcon", "style"=>"background-image:url({$disk->diskIcon})"]);
        echo Html::a("<h3>".htmlspecialchars($disk->diskTitle)."</h3>", ['view', 'id' => $disk->diskId],  ['title' => $disk->diskTitle]);
        echo '</div>';
    }
    ?>
    </div>

</div>
<style type="text/css">
    .disklistItemIcon{
        display:block;
        height:300px;
        width:300px;
        background-size:cover;
    }
    .disklistItemIcon:hover{
        opacity:0.8;
    }
    .disklistItem h3{
        text-align:center;
        white-space:nowrap;
        width:100%;
        overflow:hidden;
    }
</style>