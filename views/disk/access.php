<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Disk */

$this->title = 'Access to Disk: ' . $model->diskTitle;
if (\Yii::$app->user->identity->is('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['index']];
}
if (\Yii::$app->user->identity->is('teacher')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['teacherindex']];
}
$this->params['breadcrumbs'][] = ['label' => $model->diskTitle, 'url' => ['view', 'id' => $model->diskId]];
$this->params['breadcrumbs'][] = 'Access';
?>
<div class="disk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <h3>Group Access</h3>
    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th style="width:10%;"></th>
                <th style="width:50%;">Group</th>
                <th style="width:20%;">From Date</th>
                <th style="width:20%;">To Date</th>
            </tr>
        </thead>
        <tbody id="diskgroup">
        </tbody>
    </table>

    <h3>Add/Replace Group Access</h3>
    <div class="row">
        <div class="col-sm-4">Group:<br/>
            <?php echo Html::dropDownList('list', '', ArrayHelper::map($groups, 'groupId', 'groupName'), ['class' => 'form-control', 'id' => 'newGroupId']); ?>
        </div>
        <div class="col-sm-2">From Date:<br/><input type="text" id="fromDate" class="form-control"></div>
        <div class="col-sm-2">To Date:<br/><input type="text" id="toDate" class="form-control"></div>
    </div>
    <br/>
    <button id="btnAddGroup" class="btn btn-primary"> Add / Replace </button>














    <h3>User Access</h3>
    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th style="width:10%;"></th>
                <th style="width:50%;">User</th>
                <th style="width:20%;">From Date</th>
                <th style="width:20%;">To Date</th>
            </tr>
        </thead>
        <tbody id="diskuser">
        </tbody>
    </table>

    <h3>Add/Replace User Access</h3>
    <div class="row">
        <div class="col-sm-4">User:<br/>
        <?php
        // echo '<label class="control-label">Provinces</label>';
        echo Select2::widget([
            'name' => 'newUserId',
            'data' => ArrayHelper::map($users, 'userId', 'userFullName'),
            'options' => [
                'placeholder' => 'Select users ...',
                'multiple' => true,
                'id' => 'newUserId'
            ],
        ]);
        ?>

        </div>
        <div class="col-sm-2">From Date:<br/><input type="text" id="fromDate2" class="form-control"></div>
        <div class="col-sm-2">To Date:<br/><input type="text" id="toDate2" class="form-control"></div>
    </div>
    <br/>
    <button id="btnAddUser" class="btn btn-primary"> Add / Replace </button>




    <?php
    $this->registerJsFile('@web/js/diskgroup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('@web/js/diskuser.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    
    $this->registerJsFile('@web/js/datepicker/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile('@web/js/datepicker/css/bootstrap-datepicker.min.css');
    
    $this->registerJs(
            "   
                window.diskgroup.init({
                    diskId:{$model->diskId},
                    containerId: 'diskgroup',
                    btnAdd:'btnAddGroup'
                });
                jQuery('#fromDate').datepicker({format: 'yyyy-mm-dd'});
                jQuery('#toDate').datepicker({format: 'yyyy-mm-dd'});


                window.diskuser.init({
                    diskId:{$model->diskId},
                    containerId: 'diskuser',
                    btnAdd:'btnAddUser'
                });
                jQuery('#fromDate2').datepicker({format: 'yyyy-mm-dd'});
                jQuery('#toDate2').datepicker({format: 'yyyy-mm-dd'});

                    
            ", yii\web\View::POS_LOAD, 'my-diskrecord-handler'
    );
    $this->registerCss(
            "   
                .del, .save{ cursor:pointer; margin-right:10px;}
            ");
    ?>
</div>
