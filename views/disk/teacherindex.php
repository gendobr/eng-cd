<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DiskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn',
  
             'template' => '{view} {access} ',
             'buttons' => [
                    'access' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', 
                                      ['/disk/access', 'id' => $model->diskId], 
                                      ['title' => "Access"]);
                    },        
                    'view'=>function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 
                                ['view', 'id' => $model->diskId], 
                                ['title' => "View"]);
                    },
                ]
            ],
            // 'diskId',
            'diskTitle',
            // 'diskIcon',
            // 'diskDescription:ntext',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
