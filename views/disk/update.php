<?php

use yii\helpers\Html;
// use yii\helpers\ArrayHelper;
// use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Disk */

$this->title = 'Update Disk: ' . $model->diskTitle;
if (\Yii::$app->user->identity->is('admin')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['index']];
}
if (\Yii::$app->user->identity->is('teacher')) {
    $this->params['breadcrumbs'][] = ['label' => 'Disks', 'url' => ['teacherindex']];
}
$this->params['breadcrumbs'][] = ['label' => $model->diskTitle, 'url' => ['view', 'id' => $model->diskId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>


    <h3>Tracks</h3>
    <table class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th style="width:10%;"></th>
                <th style="width:10%;">Ordering</th>
                <th style="width:30%;">Title</th>
                <th>Mp3</th>
                <th>Ogg</th>
                <th>Wav</th>
            </tr>
        </thead>
        <tbody id="diskrecord">
        </tbody>
    </table>
    <h3>Upload one track</h3>
    <div class="row">
        <div class="col-sm-4">Title:<br/><input type="text" id="newtitle" class="form-control"></div>
        <div class="col-sm-2">Ordering:<br/><input type="text" id="neworder" class="form-control"></div>
        <div class="col-sm-2">Mp3:<br/><input type="file" id="newfileMp3"></div>
        <div class="col-sm-2">Ogg:<br/><input type="file" id="newfileOgg"></div>
        <div class="col-sm-2">Wav:<br/><input type="file" id="newfileWav"></div>
    </div>
    <br/>
    <button id="btnAdd" class="btn btn-primary">Upload</button>


    
    
    






    <?php
    $this->registerJsFile('@web/js/diskrecord.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    
    $this->registerJs(
            "   
                window.diskrecord.init({
                    diskId:{$model->diskId},
                    containerId: 'diskrecord',
                    btnAdd:'btnAdd'
                });
                
                    
            ", yii\web\View::POS_LOAD, 'my-diskrecord-handler'
    );
    $this->registerCss(
            "   
                .del, .save{ cursor:pointer; margin-right:10px;}
            ");
    ?>
</div>
