<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Useraccess */

$this->title = 'Create Useraccess';
$this->params['breadcrumbs'][] = ['label' => 'Useraccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useraccess-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
