<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Useraccess */

$this->title = 'Update Useraccess: ' . $model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Useraccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userId, 'url' => ['view', 'userId' => $model->userId, 'diskId' => $model->diskId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="useraccess-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
