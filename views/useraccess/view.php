<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Useraccess */

$this->title = $model->userId;
$this->params['breadcrumbs'][] = ['label' => 'Useraccesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="useraccess-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'userId' => $model->userId, 'diskId' => $model->diskId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'userId' => $model->userId, 'diskId' => $model->diskId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userId',
            'diskId',
            'fromDate',
            'toDate',
        ],
    ]) ?>

</div>
