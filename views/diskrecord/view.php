<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Diskrecord */

$this->title = $model->diskrecordId;
$this->params['breadcrumbs'][] = ['label' => 'Diskrecords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diskrecord-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->diskrecordId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->diskrecordId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'diskrecordId',
            'diskrecordFilePath',
            'diskrecordTitle',
            'diskId',
            'diskrecordOrdering',
        ],
    ]) ?>

</div>
