<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DiskrecordSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diskrecord-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'diskrecordId') ?>

    <?= $form->field($model, 'diskrecordFilePath') ?>

    <?= $form->field($model, 'diskrecordTitle') ?>

    <?= $form->field($model, 'diskId') ?>

    <?= $form->field($model, 'diskrecordOrdering') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
