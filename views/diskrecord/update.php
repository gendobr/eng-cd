<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diskrecord */

$this->title = 'Update Diskrecord: ' . $model->diskrecordId;
$this->params['breadcrumbs'][] = ['label' => 'Diskrecords', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->diskrecordId, 'url' => ['view', 'id' => $model->diskrecordId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="diskrecord-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
