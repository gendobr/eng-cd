<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Diskrecord */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diskrecord-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'diskrecordFilePath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diskrecordTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diskId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diskrecordOrdering')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
