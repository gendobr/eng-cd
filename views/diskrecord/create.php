<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Diskrecord */

$this->title = 'Create Diskrecord';
$this->params['breadcrumbs'][] = ['label' => 'Diskrecords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diskrecord-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
