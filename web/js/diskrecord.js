// diskrecord.js
window.diskrecord = {
        containerId: "diskrecord",
        diskId: 0,
        files:{},
        init:function(options){
            this.diskId = options.diskId;
            this.container = jQuery('#' + options.containerId);
            this.containerId = options.containerId;
            this.btnAdd=options.btnAdd;
            // Variable to store your files
            var self=this;
            // Add events
            jQuery('#'+this.btnAdd).on('click',function(event){
                // console.log('add!');
                self.add();
            });
            
            self.load();
        },
        load: function () {
            var self = this;
            jQuery.get("index.php?r=diskrecord/index&diskId=" + this.diskId, function (data, status) {
                //alert("Data: " + data + "\nStatus: " + status);
                self.renderList(data);
            });
        },
        remove: function (diskrecordId) {
            var self = this;
            console.log('deleting', diskrecordId);
            jQuery.get(
                "index.php?r=diskrecord/delete&id="+diskrecordId,
                function (data, status) { self.load(); }
            );
        },
        save: function (diskrecordId) {
            var diskrecordTitle = jQuery('#diskrecord' + diskrecordId).find('.title').first().val();
            var diskrecordOrdering = jQuery('#diskrecord' + diskrecordId).find('.order').first().val();
            var self = this;
            jQuery.post(
                "index.php?r=diskrecord/update&id="+diskrecordId,
                {
                    diskrecordId: diskrecordId,
                    diskrecordTitle: diskrecordTitle,
                    diskrecordOrdering: diskrecordOrdering
                },
                function (data, status) {
                    self.load();
                }
            );
        },
        add: function () {
            
            if(!jQuery('#newtitle').val() || !jQuery('#neworder').val() || !jQuery('#newfileMp3')[0].files[0] || !jQuery('#newfileOgg')[0].files[0] || !jQuery('#newfileWav')[0].files[0]){
                alert('Fill-in all the data');
                return;
            }

            var self = this;
            
            // Create a formdata object and add the files
            var formData = new FormData();
            formData.append('diskId',this.diskId);
            formData.append('diskrecordTitle',jQuery('#newtitle').val());
            formData.append('diskrecordOrdering',jQuery('#neworder').val());
            // console.log(data.get('diskrecordTitle'),data.get('diskrecordOrdering'));
            
            var fls;

            fls = jQuery('#newfileMp3')[0].files;
            formData.append('uploadMp3',fls[0]);
            // console.log(fls[0]);
            // console.log(data);
            
            fls = jQuery('#newfileOgg')[0].files;
            formData.append('uploadOgg',fls[0]);
            // console.log(fls[0]);
            // console.log(data);
            
            fls = jQuery('#newfileWav')[0].files;
            formData.append('uploadWav',fls[0]);
            jQuery.ajax({
                url: "index.php?r=diskrecord/create",
                //url: "i.php",
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data, textStatus, jqXHR) { 
                    jQuery('#newtitle').val('');
                    jQuery('#neworder').val('');
                    alert('Track added');
                    self.load();
                },
                error: function(jqXHR, textStatus, errorThrown){ self.load(); }
            });
        },
        renderList: function (lst) {
            // console.log(lst);
            this.container.empty();
            var list=JSON.parse(lst);
            // console.log(list);
            for (var i = 0; i < list.length; i++) {
                this.renderRow(list[i]);
            }
        },
        renderRow: function (row) {
            // console.log(row);
            var id = "diskrecord" + row.diskrecordId;
            
            var urlMp3 = row.diskrecordFilePathMp3?"<a href='"+row.diskrecordFilePathMp3+"' target='_blank'>"+row.diskrecordFilePathMp3+"</a>":'';
            var urlOgg = row.diskrecordFilePathOgg?"<a href='"+row.diskrecordFilePathOgg+"' target='_blank'>"+row.diskrecordFilePathOgg+"</a>":'';
            var urlWav = row.diskrecordFilePathWav?"<a href='"+row.diskrecordFilePathWav+"' target='_blank'>"+row.diskrecordFilePathWav+"</a>":'';
            
            var html = "<tr id='" + id + "' class='diskrecord'>" +
                       "<td><a class='del' data-id='" + row.diskrecordId + "' title='Delete'><i class='glyphicon glyphicon-trash'></i></a><a class='save' data-id='" + row.diskrecordId + "'  title='Save'><i class='glyphicon glyphicon-save'></i></a></td>" +
                       "<td><input type=text class='order form-control' value=''></td>" +
                       "<td><input type=text class='title form-control' value=''></td>" +
                       "<td>" + urlMp3 + "</td>" +
                       "<td>" + urlOgg + "</td>" +
                       "<td>" + urlWav + "</td>" +
                       "</tr>";
            var block = jQuery(html);
            // console.log(block);
            this.container.append(block);
            var self = this;
            jQuery('#' + id).find('.del').click(function () {  self.remove(jQuery(this).attr('data-id')); });
            jQuery('#' + id).find('.save').click(function () { self.save(jQuery(this).attr('data-id')); });
            jQuery('#' + id).find('.title').val(row.diskrecordTitle);
            jQuery('#' + id).find('.order').val(row.diskrecordOrdering);
        }
};

