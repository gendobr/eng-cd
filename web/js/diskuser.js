window.diskuser = {
    diskId: 0,
    init: function (options) {
        this.diskId = options.diskId;
        this.container = jQuery('#' + options.containerId);
        this.containerId = options.containerId;
        this.btnAdd = options.btnAdd;
        // Variable to store your files
        var self = this;
        // Add events
        jQuery('#' + this.btnAdd).on('click', function (event) { self.add(); });
        this.load();
    },
    load: function () {
        var self = this;
        jQuery.get("index.php?r=useraccess/diskusers&diskId=" + this.diskId, function (data, status) {
            //alert("Data: " + data + "\nStatus: " + status);
            self.renderList(data);
        });
    },
    renderList: function (lst) {
        // console.log(lst);
        this.container.empty();
        var list = JSON.parse(lst);
        //console.log(list);
        for (var i = 0; i < list.length; i++) {
            this.renderRow(list[i]);
        }
    },
    renderRow: function (row) {
        // console.log(row);

        var id = "user" + row.userId;

        var html = "<tr id='" + id + "' class='group'>" +
                "<td><a class='del' data-id='" + row.userId + "' title='Delete'><i class='glyphicon glyphicon-trash'></i></a></td>" +
                "<td>" + row.userLastName + " " + row.userLastName +", " + row.userLogin +", " + row.userEMail + "</td>" +
                "<td>" + row.fromDate + "</td>" +
                "<td>" + row.toDate + "</td>" +
                "</tr>";
        var block = jQuery(html);
        // console.log(block);
        this.container.append(block);
        // return;
        var self = this;
        jQuery('#' + id).find('.del').click(function () {
            self.remove(jQuery(this).attr('data-id'));
        });
    },
    remove: function (userId) {
        var self = this;
        console.log('deleting', userId);
        jQuery.post(
                "index.php?r=useraccess/delete&diskId=" + self.diskId + "&userId=" + userId,
                function (data, status) {
                    self.load();
                }
        );
    },
    add: function () {

        if (!jQuery('#newUserId').val() || !jQuery('#fromDate2').val() || !jQuery('#toDate2').val()) {
            alert('Fill-in all the data');
            return;
        }

        var self = this;

        // Create a formdata object and add the files
        var formData = new FormData();
        formData.append('diskId', this.diskId);
        formData.append('userId', jQuery('#newUserId').val());
        formData.append('fromDate', jQuery('#fromDate2').val());
        formData.append('toDate', jQuery('#toDate2').val());

        jQuery.ajax({
            url: "index.php?r=useraccess/create",
            //url: "i.php",
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                jQuery('#newUserId').val(null);
                jQuery('#fromDate2').val('');
                jQuery('#toDate2').val('');
                // alert('User access added');
                self.load();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.load();
            }
        });
    }
};