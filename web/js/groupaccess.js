window.groupaccess = {
    groupId: 0,
    init: function (options) {
        this.groupId = options.groupId;
        this.container = jQuery('#' + options.containerId);
        this.containerId = options.containerId;
        this.btnAdd = options.btnAdd;
        // Variable to store your files
        var self = this;
        // Add events
        jQuery('#' + this.btnAdd).on('click', function (event) {
            // console.log('add!');
            self.add();
        });


        this.load();
    },
    load: function () {
        var self = this;
        jQuery.get("index.php?r=groupaccess/index&groupId=" + this.groupId, function (data, status) {
            //alert("Data: " + data + "\nStatus: " + status);
            self.renderList(data);
        });
    },
    renderList: function (lst) {
        // console.log(lst);
        this.container.empty();
        var list = JSON.parse(lst);
        // console.log(list);
        for (var i = 0; i < list.length; i++) {
            this.renderRow(list[i]);
        }
    },
    renderRow: function (row) {
        

        var id = "disk" + row.diskId;


        var html = "<tr id='" + id + "' class='disk'>" +
                "<td><a class='del' data-id='" + row.diskId + "' title='Delete'><i class='glyphicon glyphicon-trash'></i></a></td>" +
                "<td>" + row.diskTitle + "</td>" +
                "<td>" + row.fromDate + "</td>" +
                "<td>" + row.toDate + "</td>" +
                "</tr>";

        var block = jQuery(html);
        // console.log(block);
        this.container.append(block);
        var self = this;
        jQuery('#' + id).find('.del').click(function () {
            self.remove(jQuery(this).attr('data-id'));
        });
    },
    remove: function (diskId) {
        var self = this;
        // console.log('deleting', diskId);
        jQuery.get(
                "index.php?r=groupaccess/delete&groupId=" + self.groupId + "&diskId=" + diskId,
                function (data, status) {
                    self.load();
                }
        );
    },
    add: function () {

        if (!jQuery('#newdiskId').val() || !jQuery('#fromDate').val() || !jQuery('#toDate').val()) {
            alert('Fill-in all the data');
            return;
        }

        var self = this;

        // Create a formdata object and add the files
        var formData = new FormData();
        formData.append('groupId', this.groupId);
        formData.append('diskId', jQuery('#newdiskId').val());
        formData.append('fromDate', jQuery('#fromDate').val());
        formData.append('toDate', jQuery('#toDate').val());

        jQuery.ajax({
            url: "index.php?r=groupaccess/create",
            //url: "i.php",
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                jQuery('#newdiskId').val(null);
                jQuery('#fromDate').val('');
                jQuery('#toDate').val('');
                alert('Disk access added');
                self.load();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.load();
            }
        });
    },

};