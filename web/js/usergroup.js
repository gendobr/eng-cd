window.usergroup = {
    groupId: 0,
    init: function (options) {
        this.groupId = options.groupId;
        this.container = jQuery('#' + options.containerId);
        this.containerId = options.containerId;
        this.btnAdd = options.btnAdd;
        // Variable to store your files
        var self = this;
        // Add events
        jQuery('#' + this.btnAdd).on('click', function (event) {
            // console.log('add!');
            self.add();
        });


        this.load();
    },
    load: function () {
        var self = this;
        jQuery.get("index.php?r=usergroup/index&groupId=" + this.groupId, function (data, status) {
            //alert("Data: " + data + "\nStatus: " + status);
            self.renderList(data);
        });
    },
    renderList: function (lst) {
        // console.log(lst);
        this.container.empty();
        var list = JSON.parse(lst);
        // console.log(list);
        for (var i = 0; i < list.length; i++) {
            this.renderRow(list[i]);
        }
    },
    renderRow: function (row) {
        // console.log(row);
        var id = "user" + row.userId;
        var html = "<tr id='" + id + "' class='disk'>" +
                   "<td><a class='del' data-id='" + row.userId + "' title='Delete'><i class='glyphicon glyphicon-trash'></i></a></td>" +
                   "<td>" + row.userLastName  + " " +
                            row.userFirstName + " / " +
                            row.userLogin + " " +
                            row.userEMail + " " +
                   "</td>" +
                   "</tr>";
        var block = jQuery(html);
        // console.log(block);
        this.container.append(block);
        
        var self = this;
        jQuery('#' + id).find('.del').click(function () {
            self.remove(jQuery(this).attr('data-id'));
        });
        return;
    },
    remove: function (userId) {
        var self = this;
        // console.log('deleting', diskId);
        jQuery.post(
                "index.php?r=usergroup/delete&groupId=" + self.groupId + "&userId=" + userId,
                //"index.php?r=usergroup/delete",
                {
                    // groupId:self.groupId,
                    // userId:userId,
                },
                function (data, status) {
                    self.load();
                }
        );
    },
    add: function () {

        var self = this;
        var userIds = jQuery('#newUserId').val();
        var groupId = this.groupId;

        // userId Array [ "3" ] groupId 1
        // console.log('userId', userIds, 'groupId', groupId);

        if (!userIds || !userIds.length || !groupId) {
            alert('Fill-in all the data please');
            return;
        }
        var userId;
        var formData;
        for (var i = 0; i < userIds.length; i++) {
            formData = new FormData();
            formData.append('groupId', groupId);
            formData.append('userId', userIds[i]);
            jQuery.ajax({
                url: "index.php?r=usergroup/create",
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    jQuery('#newUserId').val(null);
                    self.load();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    self.load();
                }
            });
            
        }

        return;
    }

};