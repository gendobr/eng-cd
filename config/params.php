<?php

return [
    'adminEmail' => 'admin@example.com',
    'salt' =>'dslko2094898ufm naweyr8 w yiuhu',
    'dateformat'=>'d.m.Y',
    'link_expiration_interval'=>3600, // in seconds, 1hr
    'file_server_host'=>'http://media.gaudeamus.ua:8080'
];
